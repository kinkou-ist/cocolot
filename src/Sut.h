//
// Created by jinhao on 2018-12-28.
//
/**
 * Newed:
 *      Interctions(derive, smt)
 *      Assignments(smt)
 */


#ifndef COCOLOT_SUT_H
#define COCOLOT_SUT_H

#include <iostream>
#include <vector>
#include <unordered_set>
#include <z3++.h>
#include "Parameter.h"
#include "TestCase.h"
#include "OptionHandler.h"
#include "Interaction.h"
#include "Constraint.h"

class Sut {
private:
    /**
     * Data
     */
    int t = 0, d = 0, s = 0;
    bool overlineT = false;
    bool overlineD = false;

    int paraNum = 0;
    std::vector<int>* paramToValNumVec;
    std::vector<Parameter>* paramVec;
    std::vector<std::string>* paramNameVec;

    int caseNum = 0;
    std::vector<TestCase>* testSuite;
    std::vector<std::vector<int>> testCase2InteractVec;

    int consNum = 0;
    std::vector<Constraint*>* constVec;

    int interactionNum = 0;
    std::unordered_set<Interaction*> interactSet;
    std::vector<Interaction*> interactVec;
    std::vector<std::vector<int>> interactSetVec;
    std::vector<std::vector<int>> interact2TestCaseVec;
    std::vector<std::vector<int>> interactSet2TestCaseVec;
    std::vector<std::pair<int, int>> distLogger;//if d = 1, each vector in distLogger is the pair of 2 interactions which are distinguishable; else the pair of 2 interaction set
    std::vector<std::pair<int, int>> indisLogger;

    bool printVerbosity = true;

    /**
     * Private Methods
     */
    void mkInteractions();
    void ckInteractions(z3::context& c, z3::solver& s, z3::expr_vector& vVec);
    void ckDistinguishabilitySMT(z3::context& c, z3::solver& s, z3::expr_vector& vVec);
    void checkDistinguishability();

public:
    void setParameters(std::vector<Parameter>* paramVecInst);
    void setCLAParameters(int t = 2, int d = 1, bool overlineT = false, bool overlineD = true);
    void setParamToValVec(std::vector<int>* paramToValVec);
    void setParamNameVec(std::vector<std::string>* paramNameVecInst);
    void setTestSuite(std::vector<TestCase>* testSuiteVec);
    void setCLAParameters(OptionHandler oh);
    void setConstraints(std::vector<Constraint*>* constraints);
    void setPrintVerbosity(bool repeatVerbosity);

    int getParamNum();
    std::vector<int>* getParamToValVec();
    std::vector<Parameter>* getParamVec();
    std::vector<std::string>* getParamNameVec();
    std::vector<TestCase>* getTestSuite();
    bool getOverlineD();
    std::vector<Constraint*>* getConstraints();
    std::vector<std::pair<int, int>>* getDistLogger();
    std::vector<std::pair<int, int>>* getIndisLoger();

    std::vector<Interaction*> getInteractions();
    std::vector<std::vector<int>> getInteract2CasesVec();
    std::vector<std::vector<int>> getCase2InteractsVec();
    std::vector<std::vector<int>> getInteractSetVec();

    void mkInteractionSets();
    void ckInteractionSets(bool onlyValidity);

    int updateTestSuite(std::vector<int> newTS);

};


#endif //COCOLOT_SUT_H
