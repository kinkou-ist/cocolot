//
// Created by k-kou on 18/12/08.
//

#ifndef COCOLOT_TESTCASE_H
#define COCOLOT_TESTCASE_H


#include <vector>
#include "Assignment.h"

class TestCase {
private:
    int testCaseNum = 0;
    std::vector<Assignment> assignVec;//constant value
    std::vector<int> case2InteractVec;//int array

public:
    TestCase(const int &tcNum) : testCaseNum(tcNum) {};
    void setAnAssignment(Assignment assignInst);
    int getAssignedValue(const int &paramID);
    Assignment* getAssign(const int &paramID);
    int getTestCaseID();
    void registInteractCovering(const int &interactID);
};


#endif //COCOLOT_TESTCASE_H
