//
// Created by k-kou on 18/12/08.
//

#include "TestCase.h"
#include "Printer.h"

void TestCase::setAnAssignment(Assignment assignInst) {
    this->assignVec.push_back(assignInst);
}

int TestCase::getAssignedValue(const int &paramID) {
    Assignment gotAssign = assignVec[paramID];
    if(gotAssign.getParamID() == paramID) {
        return gotAssign.getValueID();
    } else {
        return -1;
    }
}

Assignment* TestCase::getAssign(const int &paramID) {
    if(assignVec[paramID].getParamID() == paramID) {
        return &assignVec[paramID];
    } else {
        return nullptr;
    }
}

int TestCase::getTestCaseID() {
    return this->testCaseNum;
}

void TestCase::registInteractCovering(const int &interactID) {
    this->case2InteractVec.push_back(interactID);
}

