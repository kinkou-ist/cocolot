//
// Created by hao jin on 2018-11-28.
//

/**
 * The OptionHandler uses the Boost library(program_options)
 * Basically, it stores all of the information of the system arguments.
 * For value passing, maybe passing the map would work.
 */

#ifndef COCOLOT_OPTIONHANDLER_H
#define COCOLOT_OPTIONHANDLER_H

#include "Printer.h"

#define SMT_METHOD "smt"
#define DERIVE_METHOD "derive"
#define BOOL_FORMAT "bool"

class OptionHandler {
private:
    int ac;
    char** av;
    std::string inputPath = "";
    int dValue = 0;
    int tValue = 0;
    int strength = 0;
    bool Djudge = false;//false: exclude d-bar; true: include d-bar
    bool Tjudge = false;//false: exclude t-bar; true: include t-bar
    std::string method = "";
    int symmetryBreakingMethod = -1;
    int repeatTimes = 1;
    std::string convert = "";
    std::string inputFormat = "";
    bool measureCov = false;
    bool validCheck = false;
    std::string testSuitePath = "";

    void processHandling();

public:
    OptionHandler(const int& argc, char** argv);
    void printOptions();

    std::string getInputFile();
    std::string getTestSuiteFile();
    int getDValue();
    int getTValue();
    int getStrength();
    bool getDJudge();
    bool getTJudge();
    int getSymmetryBreakingMethod();
    std::string getExecutionMethod();
    int getRepeatTimes();
    bool ifConversion();
    bool ifGeneration();
    bool ifMeasureCov();
    bool ifValidCheck();
    std::string getOutputFormat();
    std::string getInputFormat();
};


#endif //COCOLOT_OPTIONHANDLER_H
