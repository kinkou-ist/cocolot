//
// Created by jinhao on 2018-12-28.
//

#include <random>
#include <stack>
#include <fstream>
#include <algorithm>
#include "z3++.h"
#include "Util.h"
#include "Printer.h"

/**
 * Current Using
 */

/**
 * Optimized minimizeTestSuite method using vector
 * @param testCase2InteractionMap
 * @param testSuite
 * @param indisLogger
 * @param interact2TestIdVector
 * @param claDbar
 * @return
 */
std::vector<int> minimizeTestSuite(std::vector<Interaction*> interactId2Instance, std::vector<std::vector<int>> testCase2InteractionVec, std::vector<std::vector<int>> interact2TestIdVector, bool claDbar, bool verbosity, int repeatItr) {
    //0 => not edited; 1 => ok to delete; -1 => should not delete
    Printer prt;                                                                                                        //for printer usage
    std::vector<int> deleteCases;                                                                                       //record the test cases that should be deleted
    std::vector<int> testSuite(testCase2InteractionVec.size());

    std::random_device rnd;
    std::mt19937 mt(rnd());
    /**
     * For test
     */
//    std::mt19937 mt(1);
    unsigned long testSuiteSize = testSuite.size();
    std::uniform_int_distribution<> randTestSuite(0, ((int)testSuite.size() - 1));//generate a random int inbetween 0 an testSuite.size - 1

    int accessedCaseCounter = 0;
    /**
     * While there are test cases that are not accessed, loop will be executed
     */
    while(accessedCaseCounter != testSuiteSize){                        //for each test case

        int testCaseToDelete = randTestSuite(mt);
        /**
         * If test case has not been accessed
         */
        if(testSuite[testCaseToDelete] == 0){                               //we've got to an unaccessed test case
            accessedCaseCounter++;                                          //record it

            auto interactIDVector = (testCase2InteractionVec)[testCaseToDelete];//get all of the interactions that are covered by the test case
            auto distInteractIdInfluenced = new std::vector<int>;

            /**
             * Delete the case from the interaction cover map
             */
            for(auto interactID : interactIDVector){
                std::vector<int>* coveringTestCaseIdVecHead = &(interact2TestIdVector[interactID]);  //the covering test case sets that are to be edited
                for(int testCasePosition = 0; testCasePosition < coveringTestCaseIdVecHead->size(); testCasePosition++) {
                    if((*coveringTestCaseIdVecHead)[testCasePosition] == testCaseToDelete) {
                        (*coveringTestCaseIdVecHead).erase((*coveringTestCaseIdVecHead).begin()+testCasePosition);//it will influence interact2TestIdVector too
                        break;
                    }
                }
                if(interactId2Instance.at(interactID)->getDistinguishability()){
                    distInteractIdInfluenced->push_back(interactID);
                }
            }


            if(checkValidity(*distInteractIdInfluenced, interact2TestIdVector, claDbar)){          //able to delete; best effect
                testSuite[testCaseToDelete] = 1;
                prt.printT("Try test case: ", verbosity);
                prt.println(testCaseToDelete, verbosity);
                prt.println("\t\t\t\t-> \033[31mDelete\033[0m", verbosity);
            }else{                                                      //not able to delete
                /**
                 * Recovery work
                 */
                testSuite[testCaseToDelete] = -1;
                for(auto recoveryInteractId : interactIDVector) {
                    auto coveringTestCaseIdVecHead = &(interact2TestIdVector[recoveryInteractId]);
                    coveringTestCaseIdVecHead->push_back(testCaseToDelete);
                    std::sort(coveringTestCaseIdVecHead->begin(), coveringTestCaseIdVecHead->end());
                }
                prt.printT("Try test case: ", verbosity);
                prt.println(testCaseToDelete, verbosity);
                prt.println("\t\t\t\t-> \033[32mReserve\033[0m", verbosity);
            }

            delete distInteractIdInfluenced;
        }
    }

    prt.printAsterriskLine(verbosity);

    return testSuite;
}

/**
 * Current Using: Check Validity of the Deletion
 * @param distInteractIdInfluenced
 * @param interactTestIdVec
 * @param indisLogger
 * @param claDbar
 * @return
 */
bool checkValidity(std::vector<int> distInteractIdInfluenced, std::vector<std::vector<int>> interactTestIdVec, bool claDbar){
    for(auto distInteractId : distInteractIdInfluenced) {//For all distinguishable interactions
        if(interactTestIdVec[distInteractId].empty() && claDbar) {//Check whether there are no test cases covering it
            return false;
        }
        for(int interactItr = 0; interactItr < interactTestIdVec.size(); interactItr++) {//For all interactions
            //may be not all interactions, use interact2Test and test2Interact to shrink the search
            if(distInteractId == interactItr) {//If the two comparing interactions are the same
                continue;
            } else {//Else
                if(interactTestIdVec[interactItr].empty() && claDbar)//If the other interaction is empty
                    return false;
                bool checkRes = false;
                if(interactTestIdVec[distInteractId].empty() && interactTestIdVec[interactItr].empty())//If both of the interactions are empty
                    return false;
                if(interactTestIdVec[distInteractId].size() == interactTestIdVec[interactItr].size()) {//If two covering sets are in the same sizes
                    for(int localItr = 0; localItr < interactTestIdVec[interactItr].size(); localItr++) {//Check whether two sets are the same
                        if(interactTestIdVec[distInteractId][localItr] == interactTestIdVec[interactItr][localItr]) {
                            checkRes = true;
                        } else {
                            checkRes = false;
                            break;
                        }
                    }
                    if(checkRes) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

Constraint* parseConstraint(std::string thisConst, std::vector<Parameter>* paramVec, std::vector<std::string>* paramNameVec) {
    if(thisConst[0] != '(') {
        Printer::printTln(thisConst, true);
    } else {
        /**
         * Get operator, the constraint is using prefix notation(Poland Notation)
         */
        int stringPlaceItr = 1;
        std::string thisOperator;

        for(; stringPlaceItr < thisConst.size(); stringPlaceItr++) {
            if(thisConst[stringPlaceItr] == '(' || thisConst[stringPlaceItr] == '[')
                break;
        }
        bool atomJudge = false;
        std::stack<int> blanketStack;
        std::stack<int> blanketSquareStack;
        std::vector<Constraint*> childrenVec;
        blanketStack.push(stringPlaceItr);
        thisOperator = thisConst.substr(1, (unsigned long)stringPlaceItr - 1);
        Printer::printTln(thisOperator, false);

        for(; stringPlaceItr < thisConst.size(); stringPlaceItr++) {
            switch(thisConst[stringPlaceItr]) {
                case '(': {
                    blanketStack.push(stringPlaceItr);
                    break;
                }
                case ')': {
                    if(blanketStack.size() == 1 && stringPlaceItr != thisConst.size() - 1) {
                        Printer::printTln("Constraint is in wrong format!", true);
                        exit(4);
                    }
                    int newConstStart = blanketStack.top();
                    blanketStack.pop();
                    if(blanketStack.size() == 1) {
                        Printer::printTln(thisConst.substr((unsigned long)newConstStart, (unsigned long)stringPlaceItr - newConstStart + 1), false);
                        Constraint* oneChild = parseConstraint(thisConst.substr((unsigned long)newConstStart, (unsigned long)stringPlaceItr - newConstStart + 1), paramVec, paramNameVec);
                        childrenVec.push_back(oneChild);
                    }
                    break;
                }
                case '[': {
                    if(blanketStack.size() == 1) {
                        blanketSquareStack.push(stringPlaceItr);
                        atomJudge = true;
                    }
                    break;
                }
                case ']': {
                    /**
                     * This is an atom proposition
                     */
                    if(blanketStack.size() == 1) {
                        if(blanketSquareStack.empty()) {
                            Printer::printTln("Constraint is in wrong format!", true);
                            exit(4);
                        }
                        int newParamStart = blanketSquareStack.top();
                        std::string thisParamName = thisConst.substr((unsigned long)newParamStart + 1, (unsigned long)stringPlaceItr - newParamStart - 1);
                        int paramId = 0;
                        for(; paramId < paramNameVec->size(); paramId++) {
                            std::string itrName = paramNameVec->at((unsigned long)paramId);
                            if(thisParamName == itrName) {
                                Constraint* oneChildParameter = new Constraint;
                                oneChildParameter->setIsParam();
                                oneChildParameter->setParam(paramId);
                                childrenVec.push_back(oneChildParameter);
                                break;
                            }
                        }
                        std::string thisValueName = thisConst.substr((unsigned long)stringPlaceItr + 1, thisConst.size() - stringPlaceItr - 2);
                        Parameter thisParameter = (*paramVec)[paramId];
                        int valueId = 0;
                        for(; valueId < thisParameter.getValueNum(); valueId++) {
                            if(thisValueName == thisParameter.getValueInstance(valueId).getValueName())
                                break;
                        }
                        Constraint* anotherChild = new Constraint;
                        anotherChild->setValue(valueId);
                        anotherChild->setIsValue();
                        childrenVec.push_back(anotherChild);
                        stringPlaceItr = (int)thisConst.size() - 1;
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }

        Constraint* returnCons = new Constraint;
        if(thisOperator == "and") {
            returnCons->setOperator(PropOperator::And);
        } else if(thisOperator == "or") {
            returnCons->setOperator(PropOperator::Or);
        } else if(thisOperator == "if") {
            returnCons->setOperator(PropOperator::If);
        } else if(thisOperator == "==") {
            returnCons->setOperator(PropOperator::Equal);
        } else if(thisOperator == "<>") {
            returnCons->setOperator(PropOperator::Unequal);
        } else if(thisOperator == "not") {
            returnCons->setOperator(PropOperator::Not);
        } else {
            Printer::printTln("Wrong Operator in Constraint!", true);
            exit(4);
        }

        if(childrenVec.size() != 1 && thisOperator == "not") {
            Printer::printTln("Wrong Operator in Constraint!", true);
            exit(4);
        } else if(thisOperator == "not") {
            returnCons->setOperant(childrenVec[0]);
            return returnCons;
        } else {
            for(auto thisOperant : childrenVec) {
                returnCons->setOperant(thisOperant);
            }
            return returnCons;
        }
    }
    return nullptr;
}

std::string printConstraint(Constraint* thisConst) {
    std::string thisOperatorString;
    if(thisConst->getThisOperator() == PropOperator::And) {
        thisOperatorString = "and";
    } else if(thisConst->getThisOperator() == PropOperator::Or) {
        thisOperatorString = "or";
    } else if(thisConst->getThisOperator() == PropOperator::If) {
        thisOperatorString = "if";
    } else if(thisConst->getThisOperator() == PropOperator::Equal) {
        thisOperatorString = "==";
    } else if(thisConst->getThisOperator() == PropOperator::Unequal) {
        thisOperatorString = "<>";
    } else if(thisConst->getThisOperator() == PropOperator::Not) {
        thisOperatorString = "not";
    }

    if(thisConst->getIsParam()) {
        return "[" + std::to_string(thisConst->getParam()) + "]";
    } else if(thisConst->getIsValue()) {
        return std::to_string(thisConst->getValue());
    } else {
        std::string returnString = "(";
        returnString += thisOperatorString + " ";
        for(int childItr = 0; childItr < thisConst->getOperants().size(); childItr++) {
            if(childItr != thisConst->getOperants().size() - 1) {
                returnString +=  printConstraint(thisConst->getOperants()[childItr]) + " ";
            } else {
                returnString += printConstraint(thisConst->getOperants()[childItr]);
            }
        }
        returnString += ")";
        return returnString;
    }

}

z3::expr parseConstToExpr(Constraint *oneConst, z3::context& thisContext, z3::expr_vector& paramVecForZ3) {

    z3::expr returnExpr = thisContext.bool_val(true);

    if(oneConst->getIsParam()) {
        returnExpr = paramVecForZ3[oneConst->getParam()];
    } else if(oneConst->getIsValue()) {
        returnExpr = thisContext.int_val(oneConst->getValue());
    } else if(oneConst->getThisOperator() == PropOperator::And) {
        int childCount = 0;
        for(auto childItr : oneConst->getOperants()) {
            z3::expr itrExpr = parseConstToExpr(childItr, thisContext, paramVecForZ3);
            if(childCount == 0) {
                childCount++;
                returnExpr = itrExpr;
            } else {
                returnExpr = returnExpr && itrExpr;
            }
        }
    } else if(oneConst->getThisOperator() == PropOperator::Or) {
        int childCount = 0;
        for(auto childItr : oneConst->getOperants()) {
            z3::expr itrExpr = parseConstToExpr(childItr, thisContext, paramVecForZ3);
            if(childCount == 0) {
                childCount++;
                returnExpr = itrExpr;
            } else {
                returnExpr = returnExpr || itrExpr;
            }
        }
    } else if(oneConst->getThisOperator() == PropOperator::If) {
        auto childrenVec = oneConst->getOperants();
        if(childrenVec.size() != 2) {
            Printer::printTln("Wrong Operator in Constraint: Too Many Operants in <if>!", true);
            exit(4);
        } else {
            z3::expr leftExpr = parseConstToExpr(childrenVec[0], thisContext, paramVecForZ3);
            z3::expr rightExpr = parseConstToExpr(childrenVec[1], thisContext, paramVecForZ3);
            returnExpr = z3::implies(leftExpr, rightExpr);
        }
    } else if(oneConst->getThisOperator() == PropOperator::Equal) {
        auto childrenVec = oneConst->getOperants();
        if(childrenVec.size() != 2) {
            Printer::printTln("Wrong Operator in Constraint: Too Many Operants in <==>!", true);
            exit(4);
        } else {
            z3::expr leftExpr = parseConstToExpr(childrenVec[0], thisContext, paramVecForZ3);
            z3::expr rightExpr = parseConstToExpr(childrenVec[1], thisContext, paramVecForZ3);
            returnExpr = (leftExpr == rightExpr);
        }
    } else if(oneConst->getThisOperator() == PropOperator::Unequal) {
        auto childrenVec = oneConst->getOperants();
        if(childrenVec.size() != 2) {
            Printer::printTln("Wrong Operator in Constraint: Too Many Operants in <!=>!", true);
            exit(4);
        } else {
            z3::expr leftExpr = parseConstToExpr(childrenVec[0], thisContext, paramVecForZ3);
            z3::expr rightExpr = parseConstToExpr(childrenVec[1], thisContext, paramVecForZ3);
            returnExpr = (leftExpr != rightExpr);
        }
    } else if(oneConst->getThisOperator() == PropOperator::Not) {
        auto childrenVec = oneConst->getOperants();
        if(childrenVec.size() != 1) {
            Printer::printTln("Wrong Operator in Constraint: Too Many Operants in <!>!", true);
            exit(4);
        } else {
            z3::expr childExpr = parseConstToExpr(childrenVec[0], thisContext, paramVecForZ3);
            returnExpr = (!childExpr);
        }
    }

    return returnExpr;
}

/**
 * Done by Matt, Stack overflow,
 * July, 1., '15.
 * URL: https://stackoverflow.com/a/31169617
 */
std::vector<std::vector<int>> cartesian( std::vector<std::vector<int> >& v ) {
    std::vector<std::vector<int>> returnVec;
    auto product = []( long long a, std::vector<int>& b ) { return a*b.size(); };
    const long long N = accumulate( v.begin(), v.end(), 1LL, product );
    std::vector<int> u(v.size());
//    for( long long n=0 ; n<N ; ++n )
    for( int n=0 ; n<N ; ++n ) {
//        lldiv_t q { n, 0 };
        div_t q { n, 0 };
        for( long long i=v.size()-1 ; 0<=i ; --i ) {
            q = div( q.quot, v[i].size() );
            u[i] = v[i][q.rem];
        }
        // Do what you want here with u.
        //for( int x : u ) std::cout << x << ' ';
        //std::cout << '\n';
        returnVec.push_back(u);
    }
    return returnVec;
}

int getSizeOfCCA(std::string CCAPath) {
    std::ifstream filePointer(CCAPath);
    if(!filePointer.is_open()) {
        Printer::printTln("Fail to open the file!", true);
        //abnormal exit: fail to open the input file returns 3
        exit(3);
    }

    std::string readLine;
    int testCaseCounter = 0;

    for(int rowNum = 0; getline(filePointer, readLine); rowNum++) {
        if(readLine[0] == '#' || rowNum == 0 || rowNum == 1) continue;
        else if(isspace(readLine[0])) continue;
        else {
            testCaseCounter++;
        }
    }

    return testCaseCounter;
}

std::vector<std::vector<int>> smtBasedGeneration(Sut* sutHead, int sizeForCCAOriginal, int symmetryBreakingMethod, bool repeatVerbosity) {
    Printer::printAsterriskLine(true);

    auto timeStartForSMTGen = std::chrono::system_clock::now();

    std::vector<std::vector<int>> finalTestSuite;
    bool satFlag = true;

    int sizeForCCA = sizeForCCAOriginal;

    while(satFlag && sizeForCCA != 0) {
        z3::config cfg;
        cfg.set("auto-config", true);
        z3::context ctxForCLAG(cfg);
        std::vector<z3::expr_vector> exprContainer;
        std::vector<z3::expr_vector> variantContainer;

        auto timeStartForLoop = std::chrono::system_clock::now();

        /**
         * Variants Part
         */
        for(int eachLine = 0; eachLine < sizeForCCA; eachLine++) {
            z3::expr_vector domainVec(ctxForCLAG);
            z3::expr_vector varVec(ctxForCLAG);
            for(auto paramItr : *sutHead->getParamVec()) {
                /**
                 * Set variants and domains
                 */
                std::string thisZ3Name = "[P" + std::to_string(paramItr.getParamID()) + "R" + std::to_string(eachLine) + "]";
                z3::expr newParamZ3 = ctxForCLAG.int_const(thisZ3Name.c_str());
                domainVec.push_back(newParamZ3 >= 0);
                domainVec.push_back(newParamZ3 < ctxForCLAG.int_val(paramItr.getValueNum()));
                varVec.push_back(newParamZ3);
            }
            exprContainer.push_back(domainVec);
            variantContainer.push_back(varVec);
        }
        /**
         * Constraints Part
         */
        for(int eachLine = 0; eachLine < sizeForCCA; eachLine++) {
            z3::expr_vector constVec(ctxForCLAG);
            for(auto constItr : *sutHead->getConstraints()) {
                constVec.push_back(parseConstToExpr(constItr, ctxForCLAG, variantContainer[eachLine]));
            }
            exprContainer.push_back(constVec);
        }
        /**
         * Covering Part
         */
        z3::expr_vector coverExprVec(ctxForCLAG);
        for(auto InteractItr : sutHead->getInteractions()) {
            //for each valid interaction
            z3::expr thisCoverExpr = ctxForCLAG.bool_val(false);
            for(int eachLine = 0; eachLine < sizeForCCA; eachLine++) {
                //for each line in CLA
                z3::expr interactExpr = ctxForCLAG.bool_val(true);
                for(auto assign : InteractItr->getAssignments()) {
                    z3::expr thisAssign = variantContainer[eachLine][assign->getParamID()] == ctxForCLAG.int_val(assign->getValueID());
                    interactExpr = interactExpr && thisAssign;
                }
                //there are at least one line that contains the interaction
                thisCoverExpr = thisCoverExpr || interactExpr;
            }
            coverExprVec.push_back(thisCoverExpr);
        }
        exprContainer.push_back(coverExprVec);
        /**
         * Distinguish Part
         */
        z3::expr_vector distExprVec(ctxForCLAG);
        for(auto distPair : *sutHead->getDistLogger()) {
            /**
             * Add Exclude Explicit Distinguishable Interactions
             */
            bool skipFlag = false;
            bool localFlag = false;
            for(int i = 0; i < sutHead->getInteractSetVec()[distPair.first].size(); i++) {
                Interaction* thisOneInteract = sutHead->getInteractions()[sutHead->getInteractSetVec()[distPair.first][i]];
                int distCount = 0;
                for(int j = 0; j < sutHead->getInteractSetVec()[distPair.second].size(); j++) {
                    Interaction* anotherInteract = sutHead->getInteractions()[sutHead->getInteractSetVec()[distPair.second][j]];
                    for(auto oneAssign : thisOneInteract->getAssignments()) {
                        for(auto anoAssign : anotherInteract->getAssignments()) {
                            if(oneAssign->getParamID() == anoAssign->getParamID() && oneAssign->getValueID() != anoAssign->getValueID()){
                                distCount++;
                                localFlag = true;
                                break;
                            }
                        }
                        if(localFlag) break;
                    }
                }
                if(distCount == sutHead->getInteractSetVec()[distPair.second].size()) {
                    skipFlag = true;
                    break;
                }
            }

            //*********************************************************
            if(!skipFlag) {
                z3::expr distExpr = ctxForCLAG.bool_val(false);
                for(int eachLine = 0; eachLine < sizeForCCA; eachLine++) {

                    z3::expr oneSetExpr = ctxForCLAG.bool_val(false);
                    for(int i = 0; i < sutHead->getInteractSetVec()[distPair.first].size(); i++) {
                        z3::expr oneInteractExpr = ctxForCLAG.bool_val(true);
                        for(auto assign : sutHead->getInteractions()[sutHead->getInteractSetVec()[distPair.first][i]]->getAssignments()) {
                            z3::expr thisAssign = variantContainer[eachLine][assign->getParamID()] == ctxForCLAG.int_val(assign->getValueID());
                            oneInteractExpr = oneInteractExpr && thisAssign;
                        }
                        if(i == 0) oneSetExpr = oneInteractExpr;
                        else oneSetExpr = oneSetExpr || oneInteractExpr;
                    }

                    z3::expr anotherSetExpr = ctxForCLAG.bool_val(false);
                    for(int i = 0; i < sutHead->getInteractSetVec()[distPair.second].size(); i++) {
                        z3::expr anotherInteractExpr = ctxForCLAG.bool_val(true);
                        for(auto assign : sutHead->getInteractions()[sutHead->getInteractSetVec()[distPair.second][i]]->getAssignments()) {
                            z3::expr thisAssign = variantContainer[eachLine][assign->getParamID()] == ctxForCLAG.int_val(assign->getValueID());
                            anotherInteractExpr = anotherInteractExpr && thisAssign;
                        }
                        if(i == 0) anotherSetExpr = anotherInteractExpr;
                        else anotherSetExpr = anotherInteractExpr || anotherSetExpr;
                    }


                    z3::expr xorExpr = (oneSetExpr && !anotherSetExpr) || (anotherSetExpr && !oneSetExpr);
                    distExpr = distExpr || xorExpr;

                }
                distExprVec.push_back(distExpr);
            }
        }
        exprContainer.push_back(distExprVec);
        /**
         * TODO: Symmetry Breakings
         */
        if(symmetryBreakingMethod == 0) {
            /**
             * Do nothing
             */
        } else if(symmetryBreakingMethod == 2) {
            /**
             * Pre-assignment
             */
            z3::expr_vector sbExprVec(ctxForCLAG);
            z3::expr sbExprOne = ctxForCLAG.bool_val(true);
            z3::expr sbExprAnoth = ctxForCLAG.bool_val(true);

            auto firstDisPair = *(sutHead->getDistLogger()->begin());
            auto firstDistInteract = sutHead->getInteractions()[sutHead->getInteractSetVec()[firstDisPair.first][0]];
            auto secondDistInteract = sutHead->getInteractions()[sutHead->getInteractSetVec()[firstDisPair.second][0]];

            for(auto assignItr : firstDistInteract->getAssignments()) {
                z3::expr thisAssign = variantContainer[0][assignItr->getParamID()] == ctxForCLAG.int_val(assignItr->getValueID());
                sbExprOne = sbExprOne && thisAssign;
            }

            for(auto assignItr : secondDistInteract->getAssignments()) {
                z3::expr thisAssign = variantContainer[1][assignItr->getParamID()] == ctxForCLAG.int_val(assignItr->getValueID());
                sbExprAnoth = sbExprAnoth && thisAssign;
            }
            sbExprVec.push_back(sbExprOne);
            sbExprVec.push_back(sbExprAnoth);
            exprContainer.push_back(sbExprVec);
        } else if(symmetryBreakingMethod == 1) {
            /**
             * Lexicographic Order
             */
        }

        /**
         * Check for Existance of CLAs
         */
        z3::solver claCheckSolver(ctxForCLAG, "QF_LIA");
        for(auto exprVec : exprContainer) {
            for(int exprItr = 0; exprItr < exprVec.size(); exprItr++) {
                claCheckSolver.add(exprVec[exprItr]);
            }
        }

        /**
         * Set Parameters for Solver
         */
        z3::params parameterForCTX(ctxForCLAG);
        parameterForCTX.set(":timeout", static_cast<unsigned>(10000));//time out in $static_cast<unsigned>(time)$ milliseconds
        claCheckSolver.set(parameterForCTX);
        if(claCheckSolver.check() == z3::sat) {
            auto timeStopForLoop = std::chrono::system_clock::now();
            auto elapsedTimeForLoop = std::chrono::duration_cast<std::chrono::milliseconds>(timeStopForLoop - timeStartForLoop).count();

            satFlag = true;
            Printer::printTln("\t[" + std::to_string(elapsedTimeForLoop) + "(ms)]\t\t" + "Check for size " + std::to_string(sizeForCCA) + " -> " + "\033[32mSAT\033[0m", repeatVerbosity);
            z3::model modelOfCLA = claCheckSolver.get_model();

            finalTestSuite.clear();

            finalTestSuite.resize((unsigned)sizeForCCA);
            for(int rowItr = 0; rowItr < sizeForCCA; rowItr++) {
                finalTestSuite[rowItr].resize(sutHead->getParamNum());
            }

            for(unsigned i = 0; i < modelOfCLA.size(); i++) {
                z3::func_decl oneFuncDecl = modelOfCLA[i];
                assert(oneFuncDecl.arity() == 0);
                std::string paramName = oneFuncDecl.name().str();
                int paramId = std::stoi(paramName.substr(paramName.find('P') + 1, paramName.find('R') - paramName.find('P')));
                int rowNum = std::stoi(paramName.substr(paramName.find('R') + 1, paramName.find(']') - paramName.find('R')));

                int valInCLA = modelOfCLA.get_const_interp(oneFuncDecl).get_numeral_int();
                finalTestSuite[rowNum][paramId] = valInCLA;
            }

        } else {
            auto timeStopForLoop = std::chrono::system_clock::now();
            auto elapsedTimeForLoop = std::chrono::duration_cast<std::chrono::milliseconds>(timeStopForLoop - timeStartForLoop).count();

            satFlag = false;
            Printer::printTln("\t[" + std::to_string(elapsedTimeForLoop) + "(ms)]\t\t" + "Check for size " + std::to_string(sizeForCCA) + " -> " + "\033[31mUNSAT(Time Out)\033[0m", repeatVerbosity);
        }

        sizeForCCA--;
    }

    if(finalTestSuite.empty()) {
        Printer::printTln("SMT based generation failed in SMT construction!", true);
        exit(6);
    }

    auto timeStopForSMTGen = std::chrono::system_clock::now();
    auto elapsedTimeForSMTGen = std::chrono::duration_cast<std::chrono::milliseconds>(timeStopForSMTGen - timeStartForSMTGen).count();

    Printer::printTln("This execution: ", true);
    Printer::printTln("\tSize: " + std::to_string(sizeForCCAOriginal) + " -> " + std::to_string((int)finalTestSuite.size()), true);
    Printer::printTln("\tTime: " + std::to_string(elapsedTimeForSMTGen) + "(ms)", true);

    Printer::printAsterriskLine(true);

    return finalTestSuite;
}

bool judgeEquality(Sut* sutOne, Sut* sutTwo, bool printVerbose) {
    Printer::printTln("[ SMT solver part ]: ", printVerbose);
    Printer::printTln("( list of interaction sets which are judged distinguishable(or indistinguishable) in some pairs )", printVerbose);
    std::vector<std::vector<std::vector<std::string>>> strPairVecOne = strVecConversion(sutOne, printVerbose);
    Printer::printTln("[ Test suite part ]: ", printVerbose);
    Printer::printTln("( list of interaction sets which are judged distinguishable(or indistinguishable) in some pairs )", printVerbose);
    std::vector<std::vector<std::vector<std::string>>> strPairVecTwo = strVecConversion(sutTwo, printVerbose);

    int pairCount = 0;
    for(auto pairItrOne : strPairVecOne) {
        for(auto pairItrAnother : strPairVecTwo) {
            bool checkParallel = (pairItrOne[0] == pairItrAnother[0]) && (pairItrOne[1] == pairItrAnother[1]);
            bool checkDiagonal = (pairItrOne[0] == pairItrAnother[1]) && (pairItrOne[1] == pairItrAnother[0]);

            if(checkDiagonal || checkParallel) {
                pairCount++;
                break;
            }
        }
    }

    if(pairCount == strPairVecOne.size())
        return true;
    else
        return false;
}

std::vector<std::vector<std::vector<std::string>>> strVecConversion(Sut* sut, bool printVerbose) {
    auto paramVec = sut->getParamVec();
    auto interactVec = sut->getInteractions();
    auto interactSetVec = sut->getInteractSetVec();
    std::vector<std::vector<std::vector<std::string>>> strVec;
    std::vector<std::pair<int, int>>* loggerHeadOne = nullptr;
    std::string distOrIndis;
    int countItr = 1;

    if(sut->getIndisLoger()->size() >= sut->getDistLogger()->size()) {
        loggerHeadOne = sut->getDistLogger();
        distOrIndis = "distinguishable";
    } else if(sut->getIndisLoger()->size() == 0) {
        loggerHeadOne = sut->getDistLogger();
        distOrIndis = "distinguishable";
    } else {
        loggerHeadOne = sut->getIndisLoger();
        distOrIndis = "indistinguishable";
    }

    std::unordered_set<int> interactSetIdSet;//set of distinguishable(indistinguishable) pair Id

    for(auto pair : *loggerHeadOne) {
        std::vector<std::vector<std::string>> pairVec;

        std::vector<std::string> itrSetOne;
        std::string strTempAll;
        for(auto interactId : interactSetVec[pair.first]) {
            std::string strTemp = "{";
            for(auto assign : interactVec[interactId]->getAssignments()) {
                std::string assignStr = "(";
                assignStr += (*paramVec)[assign->getParamID()].getParamName();
                assignStr += ",";
                assignStr += (*paramVec)[assign->getParamID()].getValueInstance(assign->getValueID()).getValueName();
                assignStr += ")";
                strTemp += assignStr;
            }
            strTemp += "}";
            strTempAll += strTemp;
            itrSetOne.push_back(strTemp);
        }
        std::sort(itrSetOne.begin(), itrSetOne.end());

        pairVec.push_back(itrSetOne);

        std::vector<std::string> itrSetTwo;
        strTempAll += " <===> ";
        for(auto interactId : interactSetVec[pair.second]) {
            std::string strTemp = "{";
            for(auto assign : interactVec[interactId]->getAssignments()) {
                std::string assignStr = "(";
                assignStr += (*paramVec)[assign->getParamID()].getParamName();
                assignStr += ",";
                assignStr += (*paramVec)[assign->getParamID()].getValueInstance(assign->getValueID()).getValueName();
                assignStr += ")";
                strTemp += assignStr;
            }
            strTemp += "}";
            strTempAll += strTemp;
            itrSetTwo.push_back(strTemp);
        }
        Printer::printTln("The " + std::to_string(countItr++) + "th " + distOrIndis + " pair is: " + strTempAll, printVerbose);
        std::sort(itrSetTwo.begin(), itrSetTwo.end());

        pairVec.push_back(itrSetTwo);
        strVec.push_back(pairVec);
    }

    Printer::printAsterriskLine(printVerbose);

    return strVec;
}
