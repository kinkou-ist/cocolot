//
// Created by hao jin on 2018-11-28.
//

/**
 * For methods that are using templates,
 * it is not possible to separate the definitions and declarations into
 * different files, such as .h file and .cpp file.
 * All of the definitions of template method should be written in the same file.
 */

#ifndef COCOLOT_PRINTER_H
#define COCOLOT_PRINTER_H

#include <iostream>
#include <chrono>

class Printer {
public:
    const static std::chrono::system_clock::time_point timeStart;
    static void println(bool verbosity);
    static void printT(bool verbosity);
    static void printAsterriskLine(bool verbosity);
    static void printPlusLine(bool verbosity);
    static void printDollarLine(bool verbosity);
    static void printHyphenLine(bool verbosity);
    static void printEqualLine(bool verbosity);
    static void printCurrentDateAndTime(bool verbosity);

    template <typename T>
    static void print(const T& t, bool verbosity) {
        if(verbosity) {
            std::cout << t;
        }
    };
    template <typename T>
    static void printT(const T& t, bool verbosity) {
        Printer::printT(verbosity);
        if(verbosity) {
            std::cout << t;
        }
    };
    template <typename T>
    static void println(const T& t, bool verbosity) {
        Printer::print(t, verbosity);
        Printer::println(verbosity);
    };
    template <typename T>
    static void printTln(const T& t, bool verbosity) {
        Printer::printT(t, verbosity);
        Printer::println(verbosity);
    };
    template <typename T>
    static void printTAll(const T *tStart, const T *tEnd, bool verbosity) {
        /**
         * Print all elements from tStart to tEnd, including both.
         */
        T* itrObj = tStart;
        T* itrObjEnd = tEnd->getNext();
        do {
            Printer::printTln(itrObj, verbosity);
            itrObj = itrObj->getNext();
        } while(itrObj != itrObjEnd);
    };
    template <typename T>
    static void printTAll(const T* tStart, bool verbosity) {
        T* itrObj = tStart;
        while(itrObj != nullptr) {
            Printer::printTln(itrObj, verbosity);
            itrObj = itrObj->getNext();
        }
    };
};


#endif //COCOLOT_PRINTER_H
