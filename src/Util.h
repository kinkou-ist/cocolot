//
// Created by jinhao on 2018-12-28.
//

#ifndef COCOLOT_UTIL_H
#define COCOLOT_UTIL_H

#include <algorithm>
#include <vector>
#include "Interaction.h"
#include "Constraint.h"
#include "Parameter.h"
#include "z3++.h"
#include "TestCase.h"
#include "Sut.h"

template <typename Iterator>
inline bool nextCombination(const Iterator first, Iterator k, const Iterator last) {
    /**
     * Credits: Thomas Draper
     */
    if((first == last) || (first == k) || (last == k))
        return false;
    Iterator itr1 = first;
    Iterator itr2 = last;
    ++itr1;
    if(last == itr1)
        return false;
    itr1 = last;
    --itr1;
    itr1 = k;
    --itr2;

    while(first != itr1) {
        if(*--itr1 < *itr2) {
            Iterator j = k;
            while(!(*itr1 < *j)) ++j;
            std::iter_swap(itr1, j);
            ++itr1;
            ++j;
            itr2 = k;
            std::rotate(itr1, j, last);
            while(last != j) {
                ++j;
                ++itr2;
            }
            std::rotate(k, itr2, last);
            return true;
        }
    }
    std::rotate(first, k, last);
    return false;
}

bool checkValidity(std::vector<int> distinguishInteract, std::vector<std::vector<int>> i2TVec, bool overlineD);

std::vector<int> minimizeTestSuite(std::vector<Interaction*> interactId2Instance, std::vector<std::vector<int>> testCase2InteractionVec, std::vector<std::vector<int>> interact2TestIdVector, bool claDbar, bool verbosity, int repeatItr);

Constraint* parseConstraint(std::string thisConst, std::vector<Parameter>* paramVec, std::vector<std::string>* paramNameVec);

std::string printConstraint(Constraint* thisConst);

std::vector<std::vector<int>> cartesian( std::vector<std::vector<int> >& v );

z3::expr parseConstToExpr(Constraint* oneConst, z3::context& thisContext, z3::expr_vector& paramVecForZ);

int getSizeOfCCA(std::string CCAPath);

std::vector<std::vector<int>> smtBasedGeneration(Sut* sutHead, int sizeForCCA, int symmetryBreakingMethod, bool repeatVerbosity);

bool judgeEquality(Sut* sutOne, Sut* sutTwo, bool printVerbose);

std::vector<std::vector<std::vector<std::string>>> strVecConversion(Sut* sut, bool printVerbose);

#endif //COCOLOT_UTIL_H
