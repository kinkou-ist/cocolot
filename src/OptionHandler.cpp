//
// Created by hao jin on 2018-11-28.
//

#include <boost/program_options.hpp>
#include <fstream>
#include "OptionHandler.h"


OptionHandler::OptionHandler(const int &argc, char **argv) {
    this->ac = argc;
    this->av = argv;

    processHandling();
}

void OptionHandler::processHandling() {
    //rename the namespace
    namespace po = boost::program_options;

    /**
     * First group of the options
     */
    //name the argument group
    //add options, first string: option name and abbreviations, set value type, second string: descriptions

    po::options_description smtBasedMethod("SMT-Based Method Options");
    smtBasedMethod.add_options()
            ("symmetry-break,b", po::value<int>()->default_value(0), "[optional] symmetry breaking method, none: 0; lexicographic order: 1; pre-assignment: 2")
            ;

    po::options_description UtilFuncOptions("Util Function Options");
    UtilFuncOptions.add_options()
            ("convert-format,C", po::value<std::string>(), "keyword for conversion format for [ model ] file, Bool File: bool")
            ("measure,M", "measure the coverage of input test suite, [ strength, input ] are also needed")
            ("validity-check,V", "check the validity of input test suite for locating ability, [ dimension-d, strength-d, overline-D, overline-T ] are also needed")
            ("input,i", po::value<std::string>(), "path to the test suite file")
            ;

    po::options_description totalOptions("General");
    totalOptions.add_options()
            ("help,h", "produce help message")
            ("version,v", "print versions")
            ("model,m", po::value<std::string>(), "path to the model file")
            ("strength,s", po::value<int>()->default_value(3), "strength of the input CCA, default: 3")
            ("dimension-d,d", po::value<int>()->default_value(1), "d for the output CLA, default: 1")
            ("strength-t,t", po::value<int>()->default_value(2), "t(strength) for the output CLA, default: 2")
            ("overline-D,D", "[optional] whether set maximum for d, default: NO; if set, switch to YES")
            ("overline-T,T", "[optional] whether set maximum for t, default: NO; if set, switch to YES")
            ("method-keyword,k", po::value<std::string>()->default_value("derive"), "keyword for executing method, SMT-Based: smt; Deriving: derive")
            ("repeat,r", po::value<int>()->default_value(1), "repeat times for execution, default: 1")
            ;

    totalOptions.add(smtBasedMethod).add(UtilFuncOptions);

    try {
        //options that are decided by the positions
        po::positional_options_description p;
        p.add("model", -1);

        po::variables_map vm;
        po::store(po::command_line_parser(ac,av).options(totalOptions).positional(p).run(), vm);

        po::notify(vm);

        //only after notifying the variables_map does the map store the values
        /**
         * TODO: need more encoding, print method is also needed more encoding
         */

        if(vm.count("help") || (ac == 1 && !vm.count("version"))) {
            std::cout << totalOptions << std::endl;
            //normal exit returns 0
            exit(0);
        } else if(vm.count("version")) {
            std::cout << "Version 0.1" << std::endl;
            exit(0);
        } else if(vm.count("convert-format")) {
            if(vm["convert-format"].as<std::string>() == BOOL_FORMAT) {
                this->convert = BOOL_FORMAT;
                this->inputPath = vm["model"].as<std::string>();
            }
        } else if(vm.count("measure")) {
            this->measureCov = true;
            this->strength = vm["strength"].as<int>();
            this->testSuitePath = vm["input"].as<std::string>();
            this->inputPath = vm["model"].as<std::string>();
        } else if(vm.count("validity-check")) {
            this->validCheck = true;
            this->tValue = vm["strength-t"].as<int>();
            this->dValue = vm["dimension-d"].as<int>();
            this->testSuitePath = vm["input"].as<std::string>();
            this->inputPath = vm["model"].as<std::string>();
            if(vm.count("overline-D")) {
                this->Djudge = true;
            }
            if(vm.count("overline-T")) {
                this->Tjudge = true;
            }
        } else if(vm.count("method-keyword")) {
            if(vm["method-keyword"].as<std::string>() == SMT_METHOD) {
                this->method = SMT_METHOD;
                this->inputPath = vm["model"].as<std::string>();
                this->dValue = vm["dimension-d"].as<int>();
                this->tValue = vm["strength-t"].as<int>();
                if(vm.count("overline-D")) {
                    this->Djudge = true;
                }
                if(vm.count("overline-T")) {
                    this->Tjudge = true;
                }
                if(vm.count("symmetry-break")) {
                    this->symmetryBreakingMethod = vm["symmetry-break"].as<int>();
                }
                if(vm.count("repeat")) {
                    this->repeatTimes = vm["repeat"].as<int>();
                }
            } else if (vm["method-keyword"].as<std::string>() == DERIVE_METHOD) {
                this->method = DERIVE_METHOD;
                this->inputPath = vm["model"].as<std::string>();
                this->strength = vm["strength"].as<int>();
                this->dValue = vm["dimension-d"].as<int>();
                this->tValue = vm["strength-t"].as<int>();
                if(vm.count("overline-D")) {
                    this->Djudge = true;
                }
                if(vm.count("overline-T")) {
                    this->Tjudge = true;
                }
                if(vm.count("repeat")) {
                    this->repeatTimes = vm["repeat"].as<int>();
                }
            } else {
                throw std::runtime_error(std::string("Wrong Method Assigned: ") + vm["method-keyword"].as<std::string>());
            }
        }  else {
            throw std::runtime_error(std::string("Use --help for detail info."));
        }

        /**
         * TODO: Maybe some self-designed exceptions
         */


    } catch(std::exception& e) {
        Printer::printTln(e.what(), true);
        //abnormal exit: wrong arguments return 1
        exit(1);
    }
}

void OptionHandler::printOptions() {

    if(this->ifValidCheck()) {
        Printer::printTln("Model file: " + this->inputPath, true);
        Printer::printTln("Test suite file: " + this->testSuitePath, true);
        std::stringstream boolStream;
        Printer::printTln("CLA d is: " + std::to_string(this->dValue), true);
        Printer::printTln("CLA t is: " + std::to_string(this->tValue), true);
        boolStream << std::boolalpha << this->Djudge;
        Printer::printTln("Include overline D: " + boolStream.str(), true);
        //clear the stream contents
        boolStream.str("");
        //clean the stream state, necessary
        boolStream.clear(std::stringstream::goodbit);
        boolStream << std::boolalpha << this->Tjudge;
        Printer::printTln("Include overline T: " + boolStream.str(), true);

        boolStream.str("");
        boolStream.clear(std::stringstream::goodbit);
    } else if(this->ifConversion()) {
        Printer::printTln("Conversion: " + this->convert, true);
        auto inputFormat = this->inputPath;
        inputFormat = inputFormat.substr(inputFormat.find_last_of("\\/") + 1);
        inputFormat = inputFormat.substr(inputFormat.find_last_of("\\.") + 1);
        this->inputFormat = inputFormat;
        Printer::printTln("Input format: " + inputFormat, true);
        Printer::printTln("Input file: " + this->inputPath, true);
    } else if(!this->method.empty()) {
        Printer::printTln("Assigned method: " + this->method, true);
        Printer::printTln("Input file: " + this->inputPath, true);
        std::stringstream boolStream;
        if(this->method == SMT_METHOD) {
            Printer::printTln("CLA d is: " + std::to_string(this->dValue), true);
            Printer::printTln("CLA t is: " + std::to_string(this->tValue), true);
            boolStream << std::boolalpha << this->Djudge;
            Printer::printTln("Include overline D: " + boolStream.str(), true);
            //clear the stream contents
            boolStream.str("");
            //clean the stream state, necessary
            boolStream.clear(std::stringstream::goodbit);
            boolStream << std::boolalpha << this->Tjudge;
            Printer::printTln("Include overline T: " + boolStream.str(), true);

            boolStream.str("");
            boolStream.clear(std::stringstream::goodbit);

            if(this->symmetryBreakingMethod == 0) {
                Printer::printTln("Symmetry breaking method: None", true);
            } else if(this->symmetryBreakingMethod == 1) {
                Printer::printTln("Symmetry breaking method: Lexicographic Order", true);
            } else if(this->symmetryBreakingMethod == 2) {
                Printer::printTln("Symmetry breaking method: Pre-assignment", true);
            } else {
                Printer::printTln("Fault in option handler: symmetry breaking method value!", true);
                //abnormal exit: wrong execution in option handling returns 2
                exit(2);
            }
        } else if(this->method == DERIVE_METHOD) {
            Printer::printTln("CCA strength is: " + std::to_string(this->strength), true);
            Printer::printTln("CLA d is: " + std::to_string(this->dValue), true);
            Printer::printTln("CLA t is: " + std::to_string(this->tValue), true);
            boolStream << std::boolalpha << this->Djudge;
            Printer::printTln("Include overline D: " + boolStream.str(), true);
            //clear the stream contents
            boolStream.str("");
            //clean the stream state, necessary
            boolStream.clear(std::stringstream::goodbit);
            boolStream << std::boolalpha << this->Tjudge;
            Printer::printTln("Include overline T: " + boolStream.str(), true);

            boolStream.str("");
            boolStream.clear(std::stringstream::goodbit);

        }
    } else if(this->ifMeasureCov()) {
        Printer::printTln("Model file: " + this->inputPath, true);
        Printer::printTln("Test suite file: " + this->testSuitePath, true);
        Printer::printTln("Strength: " + std::to_string(this->strength), true);
    } else {
        Printer::printTln("Fault in option handler!", true);
        //abnormal exit: wrong execution in option handling returns 2
        exit(2);
    }

    Printer::printEqualLine(true);

}

std::string OptionHandler::getInputFile() {
    return this->inputPath;
}

std::string OptionHandler::getTestSuiteFile() {
    return this->testSuitePath;
}

int OptionHandler::getDValue() {
    return this->dValue;
}

int OptionHandler::getTValue() {
    return this->tValue;
}

int OptionHandler::getStrength() {
    return this->strength;
}

bool OptionHandler::getDJudge() {
    return this->Djudge;
}

bool OptionHandler::getTJudge() {
    return this->Tjudge;
}

int OptionHandler::getSymmetryBreakingMethod() {
    return this->symmetryBreakingMethod;
}

std::string OptionHandler::getExecutionMethod() {
    return this->method;
}

int OptionHandler::getRepeatTimes() {
    return this->repeatTimes;
}

bool OptionHandler::ifConversion() {
    return !this->convert.empty();
}

bool OptionHandler::ifGeneration() {
    return !this->method.empty();
}

bool OptionHandler::ifMeasureCov() {
    return this->measureCov;
}

std::string OptionHandler::getOutputFormat() {
    return this->convert;
}

std::string OptionHandler::getInputFormat() {
    return this->inputFormat;
}

bool OptionHandler::ifValidCheck() {
    return this->validCheck;
}


