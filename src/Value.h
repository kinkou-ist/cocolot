//
// Created by hao jin on 2018-12-10.
//

#ifndef COCOLOT_VALUE_H
#define COCOLOT_VALUE_H

#include <iostream>

class Value {
private:
    int valueID = 0;
    std::string valueName = "";
    int paramBelonged = 0;
public:
    Value(const int &valueNum, const std::string &valueName, const int &paramNum) : valueID(valueNum), valueName(valueName), paramBelonged(paramNum) {};
    std::string getValueName() { return this->valueName; };
    int getValueID() const { return this->valueID; };
    int getParamBelonged() const { return this->paramBelonged; };
};


#endif //COCOLOT_VALUE_H
