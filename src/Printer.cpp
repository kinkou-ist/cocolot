//
// Created by hao jin on 2018-11-28.
//

#include <iostream>
#include "Printer.h"

const std::chrono::system_clock::time_point Printer::timeStart = std::chrono::system_clock::now();

void Printer::println(bool verbosity) {
    if(verbosity) {
        std::cout << std::endl;
    }
}

void Printer::printT(bool verbosity) {
    if(verbosity) {
        auto timeNow = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow - timeStart).count();
        std::cout << elapsed << " ms: ";
    }

}

void Printer::printAsterriskLine(bool verbosity) {
    Printer::printTln("***********************************************************************", verbosity);
}

void Printer::printPlusLine(bool verbosity) {
    Printer::printTln("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", verbosity);
}

void Printer::printHyphenLine(bool verbosity) {
    Printer::printTln("-----------------------------------------------------------------------", verbosity);
}

void Printer::printEqualLine(bool verbosity) {
    Printer::printTln("=======================================================================", verbosity);
}

void Printer::printDollarLine(bool verbosity) {
    Printer::printTln("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", verbosity);
}

void Printer::printCurrentDateAndTime(bool verbosity) {
    // current date/time based on current system
    time_t now = time(0);

    // convert now to string form
    char* dt = ctime(&now);
    Printer::printT(verbosity);
    if(verbosity) {
        std::cout << "Local date and time is: " << dt;
    }
}




