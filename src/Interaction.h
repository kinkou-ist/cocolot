//
// Created by haojin on 19/01/02.
//

#ifndef COCOLOT_INTERACTION_H
#define COCOLOT_INTERACTION_H


#include <vector>
#include <unordered_set>
#include "Assignment.h"

class Interaction {
private:
    int interactID = 0;
    std::vector<Assignment*> assignArray;
    std::unordered_set<int> testCaseSet;
    bool distinguishability = true;

public:
    Interaction() {};//default constructor private better
    explicit Interaction(std::vector<Assignment*> &tempArray, int interactNum);
    void addCoveringCase(int caseID);
    int getInteractID();
    std::unordered_set<int> getCoveringCases();
    void setDistinguishability(bool disBool);
    bool getDistinguishability();
    std::vector<Assignment*> getAssignments();
};


#endif //COCOLOT_INTERACTION_H
