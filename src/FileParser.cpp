//
// Created by k-kou on 18/12/08.
//

#include <fstream>
#include <algorithm>
#include "FileParser.h"
#include "Printer.h"
#include "Assignment.h"
#include "Util.h"


void CatlaParser::parse(std::string filePath) {
    /**
     * First line: info line
     *
     * First case line: read each value and make up a Parameter instance
     *
     * From second case line: read each value and add the value into corresponding Parameter instance
     *
     * For each loop, make up a TestCase instance and add it into domain caseVec
     */

    std::ifstream filePointer(filePath);
    if(!filePointer.is_open()) {
        Printer::printTln("Fail to open the file!", true);
        //abnormal exit: fail to open the input file returns 3
        exit(3);
    }
    /**
     * Parse the file
     */

    std::string readLine;
    auto separator = std::string(",");
    auto separatorLength = separator.length();
    auto offSet = std::string::size_type(0);
    std::vector<std::string> testCaseString;
    int testCaseCounter = 0;
    //for each line
    for(int rowNum = 0; getline(filePointer, readLine); rowNum++) {
        if(readLine[0] == '#' || rowNum == 0) {
            /**
             * Info line of the cit file
             */
            continue;
        } else if (rowNum == 1) {
            /**
             * Parameter name line, used to record the parameters and their sequences
             */
            while(1) {//separate the parameter line into parameter names and save
                auto pos = readLine.find(separator, offSet);
                if(pos == std::string::npos) {
                    testCaseString.push_back(readLine.substr(offSet));
                    break;
                }
                testCaseString.push_back(readLine.substr(offSet, pos - offSet));
                offSet = pos + separatorLength;
            }
            auto stringItr = testCaseString.cbegin();
            int paramCounter = 0;//is also a param ID counter
            for(; stringItr != testCaseString.cend(); stringItr++, paramCounter++) {
                Parameter newParam(paramCounter, *stringItr);//got a new parameter
                this->paramVec.push_back(newParam);
                this->paramNameVec.push_back(*stringItr);
            }
            this->paramNum = paramCounter;
        } else {
            /**
             * Value line, do same things as parameter line, record all of the values for each line
             */
            if(isspace(readLine[0]))
                continue;
            else {
                int paramItrPointer = 0;
                offSet = std::string::size_type(0);
                TestCase newTC(testCaseCounter++);
                while(1) {
                    auto pos = readLine.find(separator, offSet);
                    if(pos == std::string::npos) {
                        int finalValueID = this->paramVec[paramItrPointer].setAValue(readLine.substr(offSet));
                        Assignment finalAssign(paramItrPointer, finalValueID);
                        newTC.setAnAssignment(finalAssign);
                        break;
                    }
                    /**
                     * Read each value and add the value into corresponding Parameter instance
                     */
                    int valueID = this->paramVec[paramItrPointer].setAValue(readLine.substr(offSet, pos - offSet));
                    Assignment newAssign(paramItrPointer, valueID);
                    newTC.setAnAssignment(newAssign);
                    /**
                     * get to next value string
                     */
                    offSet = pos + separatorLength;
                    paramItrPointer++;
                }
                /**
                 * Add test case into test suite
                 */
                 this->testSuite.push_back(newTC);
                 this->caseNum++;
            }
        }
    }
    /**
     * Close the file
     */
     filePointer.close();
}

void FileParser::printSystem(bool verbose) {
    if(verbose) {
        Printer::printTln("Print System Info", true);
        Printer::printAsterriskLine(true);
        Printer::printTln("Parameters are: ", true);
        for(auto itr : this->paramVec) {
            Printer::printTln("[ " + itr.getParamName() + " ]" + " ID: " + std::to_string(itr.getParamID()) + " has " + std::to_string(itr.getValueNum()) + " values.", true);
            Printer::printT("contains values: ");
            for(int valID = 0; valID < itr.getValueNum(); valID++) {
                Printer::print(itr.getValueInstance(valID).getValueName() + "( " + std::to_string(itr.getValueInstance(valID).getValueID()) + " )", true);
                if(valID != itr.getValueNum() - 1) {
                    Printer::print(" / ", true);
                }
            }
            Printer::println(true);
        }
        Printer::printAsterriskLine(true);
    }
}

void CatlaParser::printOthers(bool verbose) {
    if(verbose) {
        Printer::printTln("Test Suite [ (t+1) - CCA ] is : ", true);
        int tcIDItr = 0;
        for(auto tc : this->testSuite) {
            Printer::printT("[ " + std::to_string(tcIDItr++) + " ]:\t\t", true);
            for(int paramItr = 0; paramItr < this->paramNum; paramItr++) {
                if(paramItr != this->paramNum - 1) {
                    Printer::print(std::to_string(tc.getAssignedValue(paramItr)) + "\t\t", true);
                } else {
                    Printer::println(std::to_string(tc.getAssignedValue(paramItr)), true);
                }
            }
        }
        Printer::printAsterriskLine(true);
    }
}

void *CatlaParser::getOtherPointer() {
    return (void*)(&this->testSuite);
}

//CatlaParser::~CatlaParser() {
//    this->caseNum = 0;
//    this->testSuite = nullptr;
//}

void SmtlaParser::parse(std::string filePath) {
    /**
     * First line: info line
     *
     * Parameter line: read each parameter name and set its values
     *
     * Constraint line: read each constraint and parse it into Constraint instances
     */

    std::ifstream filePointer(filePath);
    if(!filePointer.is_open()) {
        Printer::printTln("Fail to open the file!", true);
        //abnormal exit: fail to open the input file returns 3
        exit(3);
    }
    /**
     * Parse the file
     */
    std::string readLine;
    int paramCounter = 0;
    //for each line
    for(int rowNum = 0; getline(filePointer, readLine); rowNum++) {
        if(readLine[0] == '#' || std::isspace(readLine[0]) != 0 || readLine.empty()) {//comment line or blank line
            continue;
        } else {
            if(readLine[0] != '(') {//parameter line
                /**
                 * Parameter line, set parameters and their values
                 */
                std::string paramName = readLine.substr(0, readLine.find_first_of('('));
                paramName.erase(std::remove_if(paramName.begin(), paramName.end(), isspace), paramName.end());
                Parameter newParam(paramCounter, paramName);//got a new parameter
                this->paramVec.push_back(newParam);
                this->paramNameVec.push_back(paramName);
                int valueCounter = 0;
                std::string valueString = readLine.substr(readLine.find_first_of('(') + 1, readLine.find_first_of(')') - readLine.find_first_of('(') - 1);
                int valuePlaceStart = 0;
                for(int valuePlaceItr = 0; valuePlaceItr <= valueString.size(); valuePlaceItr++) {
                    if(isspace(valueString[valuePlaceItr]) || valuePlaceItr == valueString.size()) {
                        std::string newValue = valueString.substr((unsigned long)valuePlaceStart, (unsigned long)valuePlaceItr - valuePlaceStart);
                        this->paramVec[paramCounter].setAValue(newValue);
                        while(isspace(valueString[valuePlaceItr])) {
                            valuePlaceItr++;
                        }
                        valuePlaceStart = valuePlaceItr;
                    }
                }
                paramCounter++;
            } else {//constraint line
                readLine.erase(std::remove_if(readLine.begin(), readLine.end(), isspace), readLine.end());
                Constraint* newConstraintPtr = parseConstraint(readLine, this->getParamVec(), this->getParamNameVec());
                this->constVec.push_back(newConstraintPtr);
                this->constNum++;
            }
        }
    }
    this->paramNum = paramCounter;
    /**
     * Close the file
     */
    filePointer.close();
}

void SmtlaParser::printOthers(bool verbose) {
    if(verbose) {
        Printer::printTln("System Constraints are : ", true);
        int constraintItr = 0;
        for(auto thisConst : this->constVec) {
            Printer::printT("[ " + std::to_string(constraintItr++) + " ]:", true);
            std::string thisString = printConstraint(thisConst);
            Printer::println(thisString, true);
        }
    }
}

void *SmtlaParser::getOtherPointer() {
    return (void*)(&this->constVec);
}

void CNFParser::parse(std::string filePath) {
    /**
     * Open the input file
     */
    std::ifstream filePointer(filePath);
    if(!filePointer.is_open()) {
        Printer::printTln("Fail to open the file!", true);
        //abnormal exit: fail to open the input file returns 3
        exit(3);
    }
    /**
     * Parse the input file
     */
    this->filePath = filePath;
    int constNumInFile = 0;
    std::string readLine;
    //for each line
    for(int rowNum = 0; getline(filePointer, readLine); rowNum++) {
        if(readLine[0] == 'c' || std::isspace(readLine[0]) != 0 || readLine.empty()) {//comment line or blank line
            continue;
        } else {
            if(readLine[0] == 'p') {//parameter line
                std::string firstCnfStr = readLine.substr(readLine.find('c'));
                firstCnfStr = firstCnfStr.substr(firstCnfStr.find(' ') + 1);
                this->paramNum = std::stoi(firstCnfStr.substr(0, firstCnfStr.find(' ')));
                this->constNum = std::stoi(firstCnfStr.substr(firstCnfStr.find(' ') + 1));
                this->constStrVec.resize((unsigned long)this->constNum);
            } else {//constraint line
                std::string oneConst = "(";
                while(readLine[0] != '0') {
                    unsigned long firstSpacePos = readLine.find(' ');
                    auto oneAtom = readLine.substr(0, firstSpacePos);
                    readLine = readLine.substr(firstSpacePos + 1);

                    if(oneAtom[0] == '-') {
                        oneConst = oneConst + "!p" + oneAtom.substr(1);
                    } else {
                        oneConst = oneConst + "p" + oneAtom;
                    }

                    if(readLine[0] == '0') {
                        oneConst += ")";
                    } else {
                        oneConst += " | ";
                    }
                }
                this->constStrVec[constNumInFile] = oneConst;
                constNumInFile++;
            }
        }
    }
    /**
     * Close the file
     */
    filePointer.close();
}

void CNFParser::printOthers(bool verbose) {
    std::string convertOutputPath = this->filePath.substr(this->filePath.find_last_of('/') + 1, this->filePath.find_last_of('.') - this->filePath.find_last_of('/') - 1);
    convertOutputPath = this->currentPath + "/conversion/" + convertOutputPath + ".bool";
    std::ofstream outFile(convertOutputPath);
    for(int paramIdItr = 1; paramIdItr <= this->paramNum; paramIdItr++) {
        outFile << "$ p" + std::to_string(paramIdItr) << std::endl;
        Printer::println("$ p" + std::to_string(paramIdItr), verbose);
    }
    for(auto constItr : this->constStrVec) {
        outFile << constItr << std::endl;
        Printer::println(constItr, verbose);
    }

    outFile.close();
}

void *CNFParser::getOtherPointer() {
    return nullptr;
}

void TestSuiteParser::parse(std::string filePath) {

    std::ifstream filePointer(filePath);
    if(!filePointer.is_open()) {
        Printer::printTln("Fail to open the file!", true);
        //abnormal exit: fail to open the input file returns 3
        exit(3);
    }
    /**
     * Parse the file
     */

    std::string readLine;
    auto separator = std::string(",");
    auto separatorLength = separator.length();
    auto offSet = std::string::size_type(0);
    std::vector<std::string> testCaseString;
    int testCaseCounter = 0;
    //for each line
    for(int rowNum = 0; getline(filePointer, readLine); rowNum++) {

        if(isspace(readLine[0])) continue;
        readLine = readLine.substr(readLine.find_first_of(' ') + 1);

        if(rowNum == 0) {
            /**
             * Get parameter num
             */
            int paramItr = 0;
            while(1) {//separate the parameter line into parameter names and save
                auto pos = readLine.find(separator, offSet);
                if(pos == std::string::npos) {
                    testCaseString.push_back("p" + std::to_string(paramItr++));
                    break;
                }
                testCaseString.push_back(readLine.substr(offSet, pos - offSet));
                offSet = pos + separatorLength;
            }
            auto stringItr = testCaseString.cbegin();
            int paramCounter = 0;//is also a param ID counter
            for(; stringItr != testCaseString.cend(); stringItr++, paramCounter++) {
                Parameter newParam(paramCounter, *stringItr);//got a new parameter
                this->paramVec.push_back(newParam);
                this->paramNameVec.push_back(*stringItr);
            }
            this->paramNum = paramCounter;
        }
        /**
          * Value line, do same things as parameter line, record all of the values for each line
          */

        int paramItrPointer = 0;
        offSet = std::string::size_type(0);
        TestCase newTC(testCaseCounter++);
        while(1) {
            auto pos = readLine.find(separator, offSet);
            if(pos == std::string::npos) {
                int finalValueID = this->paramVec[paramItrPointer].setAValue(readLine.substr(offSet));
                Assignment finalAssign(paramItrPointer, finalValueID);
                newTC.setAnAssignment(finalAssign);
                break;
            }
            /**
             * Read each value and add the value into corresponding Parameter instance
             */
            int valueID = this->paramVec[paramItrPointer].setAValue(readLine.substr(offSet, pos - offSet));
            Assignment newAssign(paramItrPointer, valueID);
            newTC.setAnAssignment(newAssign);
            /**
             * get to next value string
             */
            offSet = pos + separatorLength;
            paramItrPointer++;
        }
        /**
         * Add test case into test suite
         */
        this->testSuite.push_back(newTC);
        this->caseNum++;
    }
    /**
     * Close the file
     */
    filePointer.close();
}

void TestSuiteParser::printOthers(bool verbose) {

}

void *TestSuiteParser::getOtherPointer() {
    return (void*)(&this->testSuite);
}
