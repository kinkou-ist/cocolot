//
// Created by k-kou on 18/12/08.
//

#include "Parameter.h"

void Parameter::setAllValues(std::vector<std::string> valueNameVec) {

}

int Parameter::setAValue(std::string valueName) {
    if(this->valueNameToIDMap.find(valueName) == this->valueNameToIDMap.cend()) {
        /**
         * The value has not been registered
         */
        Value newValue(this->valueNum, valueName, this->paramID);
        this->valueVec.push_back(newValue);
        this->valueNameToIDMap.insert({valueName, this->valueNum});
        /**
         * Update the value num
         */
        valueNum++;

        return valueNum - 1;
    } else {
        return this->valueNameToIDMap.at(valueName);
    }
}
