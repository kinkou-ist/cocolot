//
// Created by jinhao on 2018-12-28.
//

#include "Sut.h"
#include "Interaction.h"
#include "Util.h"
#include "z3++.h"

void Sut::setParameters(std::vector<Parameter>* paramVecInst) {
    this->paramVec = paramVecInst;
    this->paraNum = (int)paramVecInst->size();
}

void Sut::setParamToValVec(std::vector<int>* paramToValVec) {
    this->paramToValNumVec = paramToValVec;
}

void Sut::setParamNameVec(std::vector<std::string>* paramNameVecInst) {
    this->paramNameVec = paramNameVecInst;
}

void Sut::setTestSuite(std::vector<TestCase>* testSuiteVec) {
    this->testSuite = testSuiteVec;
    this->caseNum = (int)testSuiteVec->size();
}

void Sut::setPrintVerbosity(bool repeatVerbosity) {
    this->printVerbosity = repeatVerbosity;
}

int Sut::getParamNum() {
    return this->paraNum;
}

std::vector<int> *Sut::getParamToValVec() {
    return this->paramToValNumVec;
}

std::vector<Parameter> *Sut::getParamVec() {
    return this->paramVec;
}

std::vector<std::string> *Sut::getParamNameVec() {
    return this->paramNameVec;
}

std::vector<TestCase> *Sut::getTestSuite() {
    return this->testSuite;
}

std::vector<Constraint*> *Sut::getConstraints() {
    return this->constVec;
}

std::vector<std::pair<int, int>> *Sut::getDistLogger() {
    return &(this->distLogger);
}

std::vector<std::pair<int, int>> *Sut::getIndisLoger() {
    return &this->indisLogger;
}

void Sut::setCLAParameters(OptionHandler oh) {
    this->t = oh.getTValue();
    this->d = oh.getDValue();
    this->overlineT = oh.getTJudge();
    this->overlineD = oh.getDJudge();
}

void Sut::setCLAParameters(int t, int d, bool overlineT, bool overlineD) {
    this->t = t;
    this->d = d;
    this->overlineT = overlineT;
    this->overlineD = overlineD;
}

void Sut::mkInteractions() {
    std::unordered_set<Interaction*> returnSet;
    std::vector<std::vector<int>> combiStorageForInt;
    std::unordered_map<uint64_t, std::vector<Assignment*>> tempAssignSetHead;
    std::unordered_map<uint64_t, Interaction*> tempInteractionMap;

    if(this->overlineT) {
        /**
         * Get all patterns of combinations whose strength is not more than t
         */
        for(int strengthItr = 1; strengthItr <= this->t; strengthItr++) {
            auto paramNum = (int)this->paramVec->size();
            std::vector<int> combiIntVec;
            for(int i = 0; i < paramNum; combiIntVec.push_back(i++));
            do {
                std::vector<int> oneCombi;
                for(int j = 0; j < strengthItr; j++) {
                    oneCombi.push_back(combiIntVec[j]);
                }
                combiStorageForInt.push_back(oneCombi);
            } while(nextCombination(combiIntVec.begin(), combiIntVec.begin() + strengthItr, combiIntVec.end()));
        }
        /**
         * For each pattern of the parameter combinations, get the value assigns from each test case,
         * and collect them as a set of the interactions
         */
        for(auto testCaseItr : *this->testSuite) {
            std::vector<int> tempInteractVec;

            for(auto patternItr : combiStorageForInt) {
                uint64_t mapKey = 0;
                std::vector<Assignment*> tempAssignVec;
                for(auto paramInPatternItr : patternItr) {
                    tempAssignVec.push_back(testCaseItr.getAssign(paramInPatternItr));
                    mapKey = mapKey * 1000 + (paramInPatternItr + 1);
                    mapKey = mapKey * 1000 + (testCaseItr.getAssignedValue(paramInPatternItr) + 1);
                }
                auto insertIndicator = tempAssignSetHead.find(mapKey);
                if(insertIndicator == tempAssignSetHead.cend()) {
                    tempAssignSetHead.insert({mapKey, tempAssignVec});
                    Interaction* oneInteraction = new Interaction(tempAssignVec, this->interactionNum++);
                    oneInteraction->addCoveringCase(testCaseItr.getTestCaseID());
                    tempInteractionMap.insert({mapKey, oneInteraction});
                    tempInteractVec.push_back(oneInteraction->getInteractID());
                } else {
                    Interaction* existingInteract = tempInteractionMap.find(mapKey)->second;
                    existingInteract->addCoveringCase(testCaseItr.getTestCaseID());
                    tempInteractVec.push_back(existingInteract->getInteractID());
                }
            }
            this->testCase2InteractVec.push_back(tempInteractVec);
        }
    } else {
        /**
         * The same as above
         */
        auto paramNum = (int)this->paramVec->size();
        std::vector<int> combiIntVec;
        for(int i = 0; i < paramNum; combiIntVec.push_back(i++));
        do {
            std::vector<int> oneCombi;
            for(int j = 0; j < this->t; j++) {
                oneCombi.push_back(combiIntVec[j]);
            }
            combiStorageForInt.push_back(oneCombi);
        } while(nextCombination(combiIntVec.begin(), combiIntVec.begin() + this->t, combiIntVec.end()));

        for(auto testCaseItr : *this->testSuite) {
            std::vector<int> tempInteractVec;

            for(auto patternItr : combiStorageForInt) {
                uint64_t mapKey = 0;
                std::vector<Assignment*> tempAssignVec;
                for(auto paramInPatternItr : patternItr) {
                    tempAssignVec.push_back(testCaseItr.getAssign(paramInPatternItr));
                    mapKey = mapKey * 1000 + (paramInPatternItr + 1);
                    mapKey = mapKey * 1000 + (testCaseItr.getAssignedValue(paramInPatternItr) + 1);
                }
                auto insertIndicator = tempAssignSetHead.find(mapKey);
                if(insertIndicator != tempAssignSetHead.cend()) {
                    Interaction* existingInteract = tempInteractionMap.find(mapKey)->second;
                    existingInteract->addCoveringCase(testCaseItr.getTestCaseID());
                    tempInteractVec.push_back(existingInteract->getInteractID());
                } else {
                    tempAssignSetHead.insert({mapKey, tempAssignVec});
                    Interaction* oneInteraction = new Interaction(tempAssignVec, this->interactionNum++);
                    oneInteraction->addCoveringCase(testCaseItr.getTestCaseID());
                    tempInteractionMap.insert({mapKey, oneInteraction});
                    tempInteractVec.push_back(oneInteraction->getInteractID());
                }
            }
            this->testCase2InteractVec.push_back(tempInteractVec);
        }
    }

    for(auto exchangeItr = tempInteractionMap.cbegin(); exchangeItr != tempInteractionMap.cend(); exchangeItr++) {
        auto judgeItr = returnSet.insert(exchangeItr->second);
        if(!judgeItr.second) {
            Printer::printTln("Same Interaction Appeared!", true);
            exit(5);
        }
    }
    this->interactSet = returnSet;
}

void Sut::mkInteractionSets() {
    mkInteractions();
    this->interact2TestCaseVec.resize(interactSet.size());
    this->interactVec.resize(interactSet.size());
    for (auto thisInteract : this->interactSet) {
        auto coveringCases = thisInteract->getCoveringCases();
        std::vector<int> coveringCaseVec(coveringCases.size());
        std::copy(coveringCases.begin(), coveringCases.end(), coveringCaseVec.begin());
        std::sort(coveringCaseVec.begin(), coveringCaseVec.end());
        this->interact2TestCaseVec[thisInteract->getInteractID()] = coveringCaseVec;
        this->interactVec[thisInteract->getInteractID()] = thisInteract;
    }
    int interactCounter = 0;
    for (auto thisCoveringCases : this->interact2TestCaseVec) {
        Printer::printT("No.\t" + std::to_string(interactCounter++) + "\tInteraction is covered by: [\t",
                        this->printVerbosity);
        for (auto caseItr : thisCoveringCases) {
            Printer::print(std::to_string(caseItr) + "\t", this->printVerbosity);
        }
        Printer::println("]", this->printVerbosity);
    }
    Printer::printAsterriskLine(this->printVerbosity);

    checkDistinguishability();

    /**
     * TODO: make interaction set based on the d
     */

    Printer::printTln("The number of valid interactions: " + std::to_string(this->interactVec.size()), printVerbosity);
    if (this->d > 1) {
        Printer::printTln("The number of interaction sets: " + std::to_string(this->interactSetVec.size()), printVerbosity);
        Printer::printTln("The number of distinguishable set pairs: " + std::to_string((this->distLogger.size())), printVerbosity);
        Printer::printTln("The number of indistinguishable sets: " + std::to_string(this->indisLogger.size()), printVerbosity);
    } else {
        Printer::printTln("The number of distinguishable interaction pairs: " + std::to_string(this->distLogger.size()), printVerbosity);
        Printer::printTln("The number of indistinguishable interaction pairs: " + std::to_string(this->indisLogger.size()), printVerbosity);
    }

    Printer::printAsterriskLine(printVerbosity);

}

void Sut::checkDistinguishability() {

    if(this->d == 1) {
        for(int interactFirst = 0; interactFirst < interact2TestCaseVec.size(); interactFirst++) {
            for(int interactSecond = interactFirst + 1; interactSecond < interact2TestCaseVec.size(); interactSecond++) {
                bool switchForDis = false;
                if(interact2TestCaseVec[interactFirst].size() == interact2TestCaseVec[interactSecond].size()) {
                    switchForDis = true;
                    auto localPointerMax = (int)interact2TestCaseVec[interactFirst].size();
                    for(int localPointer = 0; localPointer < localPointerMax; localPointer++) {
                        if(interact2TestCaseVec[interactFirst][localPointer] == interact2TestCaseVec[interactSecond][localPointer]) {
                            switchForDis = switchForDis && true;
                        } else {
                            switchForDis = switchForDis && false;
                            break;
                        }
                    }
                }
                if(switchForDis) {//indistinguishable
                    interactVec[interactFirst]->setDistinguishability(false);
                    interactVec[interactSecond]->setDistinguishability(false);
                    std::pair<int, int> tempPair;
                    tempPair.first = (interactFirst);
                    tempPair.second = (interactSecond);
                    this->indisLogger.push_back(tempPair);
                } else {//distinguishable
                    std::pair<int, int> tempPair;
                    tempPair.first = (interactFirst);
                    tempPair.second = (interactSecond);
                    this->distLogger.push_back(tempPair);
                }
            }
//            Printer::printTln("check interaction distinguishability now" + std::to_string(interactFirst), true);
        }

        int indisCounter = 0;
        for(auto printItrIndis : this->indisLogger) {
            Printer::printT("$INDIS_PAIR$ No.\t" + std::to_string(indisCounter++) + "\twith InteractID:\t" + std::to_string(printItrIndis.first) + "\tand InteractID:\t" + std::to_string(printItrIndis.second), this->printVerbosity);
            Printer::print("\t:\t[\t", this->printVerbosity);
            for(auto casePrinterItr : interact2TestCaseVec[printItrIndis.first]) {
                Printer::print(casePrinterItr, this->printVerbosity);
                Printer::print("\t", this->printVerbosity);
            }
            Printer::println("]", this->printVerbosity);
        }
        Printer::printAsterriskLine(this->printVerbosity);

        for(int i = 0; i < this->interactVec.size(); i++) {
            std::vector<int> tempVec(1);
            tempVec.push_back(i);
            this->interactSetVec.push_back(tempVec);
        }

    } else {
        int numOfDistPair = 0;
        int numOfIndisPair = 0;

        auto interactNum = (int)this->interactVec.size();
        std::vector<int> interactIdVec;
        for(int i = 0; i < interactNum; interactIdVec.push_back(i++));

        if(this->overlineD) {
            for(int dITr = 1; dITr <= this->d; dITr++) {
                do {
                    std::vector<int> oneSet;
                    for(int j = 0; j < dITr; j++) {
                        oneSet.push_back(interactIdVec[j]);
                    }

                    bool insertStopper = false;
                    if(this->overlineT) {
                        for(int interactItrThis = 0; interactItrThis < oneSet.size(); interactItrThis++) {
                            for(int interactItrThat = interactItrThis + 1; interactItrThat < oneSet.size(); interactItrThat++) {
                                auto thisAssignVec = this->getInteractions()[oneSet[interactItrThis]]->getAssignments();
                                auto thatAssignVec = this->getInteractions()[oneSet[interactItrThat]]->getAssignments();
                                std::vector<Assignment*> bigAssignVec;
                                std::vector<Assignment*> smallAssignVec;
                                if(thisAssignVec.size() > thatAssignVec.size()) {
                                    bigAssignVec = thisAssignVec;
                                    smallAssignVec = thatAssignVec;
                                } else {
                                    bigAssignVec = thatAssignVec;
                                    smallAssignVec = thisAssignVec;
                                }

                                int existCount = 0;
                                for(auto assign : smallAssignVec) {
                                    bool existCheck = false;
                                    for(auto assignRefer : bigAssignVec) {
                                        existCheck = existCheck || (assign->getParamID() == assignRefer->getParamID() && assign->getValueID() == assignRefer->getValueID());
                                        if(existCheck)
                                            break;
                                    }
                                    if(existCheck)
                                        existCount++;
                                }
                                if(existCount == smallAssignVec.size()) {
                                    //dependent
                                    insertStopper = true;
                                    break;
                                }
                            }
                            if(insertStopper)
                                break;
                        }
                    }
                    if(!insertStopper)
                        this->interactSetVec.push_back(oneSet);
                } while(nextCombination(interactIdVec.begin(), interactIdVec.begin() + dITr, interactIdVec.end()));
            }
        } else {
            do {
                std::vector<int> oneSet;
                for(int j = 0; j < this->d; j++) {
                    oneSet.push_back(interactIdVec[j]);
                }

                bool insertStopper = false;
                if(this->overlineT) {
                    for(int interactItrThis = 0; interactItrThis < oneSet.size(); interactItrThis++) {
                        for(int interactItrThat = interactItrThis + 1; interactItrThat < oneSet.size(); interactItrThat++) {
                            auto thisAssignVec = this->getInteractions()[oneSet[interactItrThis]]->getAssignments();
                            auto thatAssignVec = this->getInteractions()[oneSet[interactItrThat]]->getAssignments();
                            std::vector<Assignment*> bigAssignVec;
                            std::vector<Assignment*> smallAssignVec;
                            if(thisAssignVec.size() > thatAssignVec.size()) {
                                bigAssignVec = thisAssignVec;
                                smallAssignVec = thatAssignVec;
                            } else {
                                bigAssignVec = thatAssignVec;
                                smallAssignVec = thisAssignVec;
                            }

                            int existCount = 0;
                            for(auto assign : smallAssignVec) {
                                bool existCheck = false;
                                for(auto assignRefer : bigAssignVec) {
                                    existCheck = existCheck || (assign->getParamID() == assignRefer->getParamID() && assign->getValueID() == assignRefer->getValueID());
                                    if(existCheck)
                                        break;
                                }
                                if(existCheck)
                                    existCount++;
                            }
                            if(existCount == smallAssignVec.size()) {
                                //dependent
                                insertStopper = true;
                                break;
                            }
                        }
                        if(insertStopper)
                            break;
                    }
                }
                if(!insertStopper)
                    this->interactSetVec.push_back(oneSet);
            } while(nextCombination(interactIdVec.begin(), interactIdVec.begin() + this->d, interactIdVec.end()));
        }

        int interactSetPos = 0;
        for(auto interactSetItr : this->interactSetVec) {
            std::vector<int> coveringCasesOne;
            for(auto interactIdItr : interactSetItr) {
                std::vector<int> tempCaseSet = coveringCasesOne;
                coveringCasesOne.clear();
                std::set_union(std::begin(this->interact2TestCaseVec[interactIdItr]), std::end(this->interact2TestCaseVec[interactIdItr]),
                               std::begin(tempCaseSet), std::end(tempCaseSet),
                               std::inserter(coveringCasesOne, std::end(coveringCasesOne)));
            }
            std::sort(coveringCasesOne.begin(), coveringCasesOne.end());
            this->interactSet2TestCaseVec.push_back(coveringCasesOne);
            interactSetPos++;
        }

        int oneSetLocate = 0, anotherSetLocate = 0;
        for(auto oneInteractSetItr = this->interactSetVec.begin(); std::next(oneInteractSetItr, 1) != this->interactSetVec.end(); oneInteractSetItr++) {
            anotherSetLocate = oneSetLocate + 1;
            for(auto anotherInteractSetItr = std::next(oneInteractSetItr, 1); anotherInteractSetItr != this->interactSetVec.end(); anotherInteractSetItr++) {
                bool switchForDis = false;
                if(this->interactSet2TestCaseVec[oneSetLocate].size() == this->interactSet2TestCaseVec[anotherSetLocate].size()) {
                    switchForDis = true;
                    auto localPointerMax = (int)this->interactSet2TestCaseVec[oneSetLocate].size();
                    for(int localPointer = 0; localPointer < localPointerMax; localPointer++) {
                        if(this->interactSet2TestCaseVec[oneSetLocate][localPointer] == this->interactSet2TestCaseVec[anotherSetLocate][localPointer]) {
                            switchForDis = switchForDis && true;
                        } else {
                            switchForDis = false;
                            break;
                        }
                    }
                }
                if(switchForDis) {//indistinguishable
                    numOfIndisPair++;
                    std::pair<int, int> indisSetPair;
                    indisSetPair.first = (oneSetLocate);
                    indisSetPair.second = (anotherSetLocate);
                    this->indisLogger.push_back(indisSetPair);
                } else {
                    numOfDistPair++;
                    std::pair<int, int> distSetPair;
                    distSetPair.first = (oneSetLocate);
                    distSetPair.second = (anotherSetLocate);
//                    std::sort(distSetPair[0], distSetPair[1]);
                    if(distSetPair.first > distSetPair.second) {
                        int temp = distSetPair.second;
                        distSetPair.second = distSetPair.first;
                        distSetPair.first = temp;
                    }
                    this->distLogger.push_back(distSetPair);
                }
                anotherSetLocate++;
            }
            oneSetLocate++;
        }
    }
}

std::vector<Interaction *> Sut::getInteractions() {
    return this->interactVec;
}

std::vector<std::vector<int>> Sut::getInteract2CasesVec() {
    return this->interact2TestCaseVec;
}

std::vector<std::vector<int>> Sut::getCase2InteractsVec() {
    return this->testCase2InteractVec;
}

std::vector<std::vector<int>> Sut::getInteractSetVec() {
    return this->interactSetVec;
}

bool Sut::getOverlineD() {
    return this->overlineD;
}

int Sut::updateTestSuite(std::vector<int> newTS) {
    Printer::printTln("Print out CLA test suite:", this->printVerbosity);
    Printer::printAsterriskLine(this->printVerbosity);
    int caseToPrintCounter = 0;
    int caseTotalPrintCounter = 0;

    Printer::printTln("Test Suite[ CLA ] is : ", this->printVerbosity);
    for(auto tc : *this->testSuite) {
        if(newTS[caseTotalPrintCounter] == -1) {
            Printer::printT("[\t" + std::to_string(caseToPrintCounter) + "\t](\t" + std::to_string(caseTotalPrintCounter) + "\t):\t\t", this->printVerbosity);
            for(int paramItr = 0; paramItr < this->paraNum; paramItr++) {
                if(paramItr != this->paraNum - 1) {
                    Printer::print(std::to_string(tc.getAssignedValue(paramItr)) + "\t\t", this->printVerbosity);
                } else {
                    Printer::println(std::to_string(tc.getAssignedValue(paramItr)), this->printVerbosity);
                }
            }
            caseToPrintCounter++;
        }
        caseTotalPrintCounter++;
    }

    std::vector<int> deleteCasesVec;
    int caseId = 0;
    for(auto caseProp : newTS) {
        if(caseProp == 1) {
            deleteCasesVec.push_back(caseId);
        }
        caseId++;
    }
    std::sort(deleteCasesVec.begin(), deleteCasesVec.end());
    int interactIdItr = 0;
    for(auto caseVecForInteract : this->interact2TestCaseVec) {
        std::vector<int> reservedCases;
        std::set_difference(
                caseVecForInteract.begin(), caseVecForInteract.end(),
                deleteCasesVec.begin(), deleteCasesVec.end(),
                std::back_inserter(reservedCases)
                );
        this->interact2TestCaseVec[interactIdItr++] = reservedCases;
    }
    Printer::printAsterriskLine(this->printVerbosity);
    int i= 0;
    for(auto printItr : this->interact2TestCaseVec){
        Printer::printT("Interaction No. ", this->printVerbosity);
        Printer::print(i++, this->printVerbosity);
        Printer::print(" is included by: [    ", this->printVerbosity);
        for(auto numCounter : printItr){
            Printer::print(numCounter, this->printVerbosity);
            Printer::print("    ", this->printVerbosity);
        }
        Printer::println("]", this->printVerbosity);
    }
    Printer::printAsterriskLine(this->printVerbosity);
    int indisCounter = 0;
    for(auto printItrIndis : this->indisLogger){
        Printer::printT("INDISTINGUISHABLE No.", this->printVerbosity);
        Printer::print(indisCounter++, this->printVerbosity);
        Printer::print(": Interaction No.", this->printVerbosity);
        Printer::print(printItrIndis.first, this->printVerbosity);
        Printer::print(" and Interaction No. ", this->printVerbosity);
        Printer::print(printItrIndis.second, this->printVerbosity);
        Printer::print(" : [    ", this->printVerbosity);
        for(auto casePrintItr : this->interact2TestCaseVec[printItrIndis.first]){
            Printer::print(casePrintItr, this->printVerbosity);
            Printer::print("    ", this->printVerbosity);
        }
        Printer::println("]", this->printVerbosity);
    }
    Printer::printAsterriskLine(this->printVerbosity);
    Printer::printT("Deleted test cases are: [    ", this->printVerbosity);
    caseTotalPrintCounter = 0;
    for(auto caseToPrint : newTS){
        if(caseToPrint == 1){
            Printer::print("\033[031m" + std::to_string(caseTotalPrintCounter) + "\033[0m", this->printVerbosity);
            Printer::print("    ", this->printVerbosity);
        }
        caseTotalPrintCounter++;
    }
    Printer::println("]", this->printVerbosity);
    Printer::printAsterriskLine(this->printVerbosity);

    return (int)(this->testSuite->size() - deleteCasesVec.size());
}

void Sut::setConstraints(std::vector<Constraint *>* constraints) {
    this->constVec = constraints;
    this->consNum = (int)constraints->size();
}

void Sut::ckInteractionSets(bool onlyValidity) {
    /**
     * Z3 Configurations
     */
    z3::config cfg;
    cfg.set("auto-config", true);//set configurations for context
    z3::context contextForCheckSUT(cfg);//set configuration
    z3::expr_vector exprVecForZ3(contextForCheckSUT);
    z3::expr_vector variantVecForZ3(contextForCheckSUT);

    /**
     * Set domains for each parameters
     */
    for(auto paramItr : *this->paramVec) {
        std::string z3Name = "[" + std::to_string(paramItr.getParamID()) + "]";//generated in paramID order
        z3::expr newParamZ3 = contextForCheckSUT.int_const(z3Name.c_str());
        exprVecForZ3.push_back(newParamZ3 >= 0);
        exprVecForZ3.push_back(newParamZ3 < paramItr.getValueNum());
        variantVecForZ3.push_back(newParamZ3);
    }

    /**
     * Construct constraints for Z3
     */
    for(auto constItr : *this->constVec) {
        exprVecForZ3.push_back(parseConstToExpr(constItr, contextForCheckSUT, variantVecForZ3));
    }

    /**
     * Construct a solver;
     */
    z3::solver solverForCheckSUT(contextForCheckSUT, "QF_LIA");

    for(int exprCounter = 0; exprCounter < exprVecForZ3.size(); exprCounter++) {
        solverForCheckSUT.add(exprVecForZ3[exprCounter]);
    }

    /**
     * Check Interaction Validity
     */
    auto timeStart = std::chrono::system_clock::now();
    ckInteractions(contextForCheckSUT, solverForCheckSUT, variantVecForZ3);
    auto timeNow = std::chrono::system_clock::now();
    auto timeForValidityCheck = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow - timeStart).count();

    this->interactVec.resize(this->interactSet.size());
    for(auto interactItr : this->interactSet) {
        this->interactVec[interactItr->getInteractID()] = interactItr;
    }

    Printer::printTln("Time for checking valid interactions: " + std::to_string(timeForValidityCheck) + "(ms)", true);

    if(!onlyValidity) {
        auto timeStartForDist = std::chrono::system_clock::now();
        ckDistinguishabilitySMT(contextForCheckSUT, solverForCheckSUT, variantVecForZ3);
        auto timeNowForDist = std::chrono::system_clock::now();
        auto timeForDistCheck = std::chrono::duration_cast<std::chrono::milliseconds>(timeNowForDist - timeStartForDist).count();
        Printer::printTln("Time for checking distinguishable interaction pairs: " + std::to_string(timeForDistCheck) + "(ms)", true);
    }
}

void Sut::ckInteractions(z3::context& c, z3::solver& s, z3::expr_vector& vVec) {
    /**
     * Get all patterns of parameter combinations
     */
    std::vector<std::vector<int>> paramCombiVec;
    std::vector<std::vector<std::vector<int>>> valueCombiVec;
    if(!this->overlineT) {//not overline on T
        auto paramNum = (int)this->paramVec->size();
        std::vector<int> combiIntVec;
        for(int i = 0; i < paramNum; combiIntVec.push_back(i++));//paramIDs: 0, 1, 2, ...
        do {
            std::vector<int> oneCombi;
            for(int j = 0; j < this->t; j++) {
                oneCombi.push_back(combiIntVec[j]);
            }
            paramCombiVec.push_back(oneCombi);
        } while(nextCombination(combiIntVec.begin(), combiIntVec.begin() + this->t, combiIntVec.end()));

        for(int paramCombiItr = 0; paramCombiItr < paramCombiVec.size(); paramCombiItr++) {
            std::vector<std::vector<int>> valueCountVec((unsigned long)this->t);
            for(int paramIdItr = 0; paramIdItr < paramCombiVec[paramCombiItr].size(); paramIdItr++) {
                std::vector<int> oneRange;
                for(int valueIdItr = 0; valueIdItr < this->paramVec->at((unsigned long)paramCombiVec[paramCombiItr][paramIdItr]).getValueNum(); valueIdItr++) {
                    oneRange.push_back(valueIdItr);
                }
                valueCountVec[paramIdItr] = oneRange;
            }
            valueCombiVec.push_back(cartesian(valueCountVec));
        }
    } else if(this->overlineT) {
        /**
         * Get all patterns of combinations whose strength is not more than t
         */
        for(int strengthItr = 1; strengthItr <= this->t; strengthItr++) {
            auto paramNum = (int)this->paramVec->size();
            std::vector<int> combiIntVec;
            for(int i = 0; i < paramNum; combiIntVec.push_back(i++));
            do {
                std::vector<int> oneCombi;
                for(int j = 0; j < strengthItr; j++) {
                    oneCombi.push_back(combiIntVec[j]);
                }
                paramCombiVec.push_back(oneCombi);
            } while(nextCombination(combiIntVec.begin(), combiIntVec.begin() + strengthItr, combiIntVec.end()));
        }

        for(int paramCombiItr = 0; paramCombiItr < paramCombiVec.size(); paramCombiItr++) { //for all kinds of parameter combinations, paramCombiVec[paramCombiItr].size -> [1, t]
                std::vector<std::vector<int>> valueCountVec((unsigned long)paramCombiVec[paramCombiItr].size());
                for(int paramIdItr = 0; paramIdItr < paramCombiVec[paramCombiItr].size(); paramIdItr++) {
                    std::vector<int> oneRange;
                    for(int valueIdItr = 0; valueIdItr < this->paramVec->at((unsigned long)paramCombiVec[paramCombiItr][paramIdItr]).getValueNum(); valueIdItr++) {
                        oneRange.push_back(valueIdItr);
                    }
                    valueCountVec[paramIdItr] = oneRange;
                }
                valueCombiVec.push_back(cartesian(valueCountVec));
        }

    }

    /**
     * Validity checking for interactions
     */
    int numOfValidInteract = 0;
    int numOfInvalidInteract = 0;
    for(int paramVecItr = 0; paramVecItr < valueCombiVec.size(); paramVecItr++) {//param combination patters
        for(int valueVecItr = 0; valueVecItr < valueCombiVec[paramVecItr].size(); valueVecItr++) {//corresponding value combination patterns
            /**
             * Set a breakpoint for repeat usage of validity checking of interactions
             */
            s.push();
            /**
             * This is an interaction
             */
            z3::expr interactExpr = c.bool_val(true);
            for(int assignItr = 0; assignItr < valueCombiVec[paramVecItr][valueVecItr].size(); assignItr++) {
                z3::expr thisAssign = vVec[paramCombiVec[paramVecItr][assignItr]] == c.int_val(valueCombiVec[paramVecItr][valueVecItr][assignItr]);
                interactExpr = interactExpr && thisAssign;
            }
            s.add(interactExpr);
            if(s.check() == z3::sat) {
                numOfValidInteract++;
                std::vector<Assignment*> assignVec;
                for(int assignItr = 0; assignItr < valueCombiVec[paramVecItr][valueVecItr].size(); assignItr++) {
                    int thisParamId = paramCombiVec[paramVecItr][assignItr];
                    int thisValueId = valueCombiVec[paramVecItr][valueVecItr][assignItr];
                    assignVec.push_back(new Assignment(thisParamId, thisValueId));
                }
                Interaction* oneInteraction = new Interaction(assignVec, this->interactionNum++);
                auto judgeItr = this->interactSet.insert(oneInteraction);
                if(!judgeItr.second) {
                    Printer::printTln("Same Interaction Appeared!", true);
                    exit(5);
                }
            } else {
                numOfInvalidInteract++;
            }
            s.pop();
        }
    }

    /**
     * Maybe need checking time?
     */


    /**
     * In order to separate validity checking and distinguishability checking, the SMT solver and context should be separated.
     * Maybe another class ?
     */

    Printer::printAsterriskLine(true);
    Printer::printTln("The number of valid interactions: " + std::to_string(numOfValidInteract), true);
    Printer::printTln("The number of invalid interactions: " + std::to_string(numOfInvalidInteract), true);

}

void Sut::ckDistinguishabilitySMT(z3::context &c, z3::solver& s, z3::expr_vector& vVec) {

    int numOfDistPair = 0;
    int numOfIndisPair = 0;

    if(this->d == 1) {
        for(auto oneInteractItr = this->interactVec.begin(); std::next(oneInteractItr, 1) != interactVec.end(); oneInteractItr++) {
            for(auto anotherInteractItr = std::next(oneInteractItr, 1); anotherInteractItr != interactVec.end(); anotherInteractItr++) {
                s.push();
                z3::expr interactExprOne = c.bool_val(true);
                Interaction* oneInteractPointer = *oneInteractItr;
                auto oneAssignmentVec = oneInteractPointer->getAssignments();
                for(auto oneAssignment = oneAssignmentVec.begin(); oneAssignment != oneAssignmentVec.end(); oneAssignment++) {
                    int thisParamID = (*oneAssignment)->getParamID();
                    int thisValueID = (*oneAssignment)->getValueID();
                    z3::expr thisAssign = vVec[thisParamID] == c.int_val(thisValueID);
                    interactExprOne = interactExprOne && thisAssign;
                }
                z3::expr interactExprAnother = c.bool_val(true);
                Interaction* anotherInteractPointer = *anotherInteractItr;
                auto anotherAssignmentVec = anotherInteractPointer->getAssignments();
                for(auto anotherAssignment = anotherAssignmentVec.begin(); anotherAssignment != anotherAssignmentVec.end(); anotherAssignment++) {
                    int thisParamID = (*anotherAssignment)->getParamID();
                    int thisValueID = (*anotherAssignment)->getValueID();
                    z3::expr thisAssignAnother = vVec[thisParamID] == c.int_val(thisValueID);
                    interactExprAnother = interactExprAnother && thisAssignAnother;
                }
                s.add((interactExprAnother && !interactExprOne) || (interactExprOne && !interactExprAnother));

                if(s.check() == z3::sat) {
                    numOfDistPair++;
                    std::pair<int, int> distPair;
                    distPair.first = ((*oneInteractItr)->getInteractID());
                    distPair.second = ((*anotherInteractItr)->getInteractID());
//                    std::sort(distPair[0], distPair[1]);
                    if(distPair.first > distPair.second) {
                        int temp = distPair.second;
                        distPair.second = distPair.first;
                        distPair.first = temp;
                    }
                    this->distLogger.push_back(distPair);
                } else {
                    numOfIndisPair++;
                    (*oneInteractItr)->setDistinguishability(false);
                    (*anotherInteractItr)->setDistinguishability(false);
                    std::pair<int, int> indistPair;
                    indistPair.first = ((*oneInteractItr)->getInteractID());
                    indistPair.second = ((*anotherInteractItr)->getInteractID());
                    this->indisLogger.push_back(indistPair);
                }
                s.pop();
            }
        }

        for(int i = 0; i < this->interactVec.size(); i++) {
            std::vector<int> tempSet;
            tempSet.push_back(i);
            this->interactSetVec.push_back(tempSet);
        }

    } else {

        auto interactNum = (int)this->interactVec.size();
        std::vector<int> interactIdVec;
        for(int i = 0; i < interactNum; interactIdVec.push_back(i++));

        if(this->overlineD) {
            for(int dItr = 1; dItr <= this->d; dItr++) {
                do {
                    std::vector<int> oneSet;
                    for(int j = 0; j < dItr; j++) {
                        oneSet.push_back(interactIdVec[j]);
                    }
                    bool insertStopper = false;
                    if(this->overlineT) {
                        for(int interactItrThis = 0; interactItrThis < oneSet.size(); interactItrThis++) {
                            for(int interactItrThat = interactItrThis + 1; interactItrThat < oneSet.size(); interactItrThat++) {
                                auto thisAssignVec = this->getInteractions()[oneSet[interactItrThis]]->getAssignments();
                                auto thatAssignVec = this->getInteractions()[oneSet[interactItrThat]]->getAssignments();
                                std::vector<Assignment*> bigAssignVec;
                                std::vector<Assignment*> smallAssignVec;
                                if(thisAssignVec.size() > thatAssignVec.size()) {
                                    bigAssignVec = thisAssignVec;
                                    smallAssignVec = thatAssignVec;
                                } else {
                                    bigAssignVec = thatAssignVec;
                                    smallAssignVec = thisAssignVec;
                                }

                                int existCount = 0;
                                for(auto assign : smallAssignVec) {
                                    bool existCheck = false;
                                    for(auto assignRefer : bigAssignVec) {
                                        existCheck = existCheck || (assign->getParamID() == assignRefer->getParamID() && assign->getValueID() == assignRefer->getValueID());
                                        if(existCheck)
                                            break;
                                    }
                                    if(existCheck)
                                        existCount++;
                                }
                                if(existCount == smallAssignVec.size()) {
                                    //dependent
                                    insertStopper = true;
                                    break;
                                }
                            }
                            if(insertStopper)
                                break;
                        }
                    }
                    if(!insertStopper)
                        this->interactSetVec.push_back(oneSet);
                } while(nextCombination(interactIdVec.begin(), interactIdVec.begin() + dItr, interactIdVec.end()));
            }
        } else {
            do {
                std::vector<int> oneSet;
                for(int j = 0; j < this->d; j++) {
                    oneSet.push_back(interactIdVec[j]);
                }

                bool insertStopper = false;
                if(this->overlineT) {
                    for(int interactItrThis = 0; interactItrThis < oneSet.size(); interactItrThis++) {
                        for(int interactItrThat = interactItrThis + 1; interactItrThat < oneSet.size(); interactItrThat++) {
                            auto thisAssignVec = this->getInteractions()[oneSet[interactItrThis]]->getAssignments();
                            auto thatAssignVec = this->getInteractions()[oneSet[interactItrThat]]->getAssignments();
                            std::vector<Assignment*> bigAssignVec;
                            std::vector<Assignment*> smallAssignVec;
                            if(thisAssignVec.size() > thatAssignVec.size()) {
                                bigAssignVec = thisAssignVec;
                                smallAssignVec = thatAssignVec;
                            } else {
                                bigAssignVec = thatAssignVec;
                                smallAssignVec = thisAssignVec;
                            }

                            int existCount = 0;
                            for(auto assign : smallAssignVec) {
                                bool existCheck = false;
                                for(auto assignRefer : bigAssignVec) {
                                    existCheck = existCheck || (assign->getParamID() == assignRefer->getParamID() && assign->getValueID() == assignRefer->getValueID());
                                    if(existCheck)
                                        break;
                                }
                                if(existCheck)
                                    existCount++;
                            }
                            if(existCount == smallAssignVec.size()) {
                                //dependent
                                insertStopper = true;
                                break;
                            }
                        }
                        if(insertStopper)
                            break;
                    }
                }
                if(!insertStopper)
                    this->interactSetVec.push_back(oneSet);
            } while(nextCombination(interactIdVec.begin(), interactIdVec.begin() + this->d, interactIdVec.end()));
        }

        int oneSetLocate = 0, anotherSetLocate = 0;
        for(auto oneInteractSetItr = this->interactSetVec.begin(); std::next(oneInteractSetItr, 1) != this->interactSetVec.end(); oneInteractSetItr++) {
            anotherSetLocate = oneSetLocate + 1;
            for(auto anotherInteractSetItr = std::next(oneInteractSetItr, 1); anotherInteractSetItr != this->interactSetVec.end(); anotherInteractSetItr++) {
                s.push();
                z3::expr interactSetExprOne = c.bool_val(false);
                for(auto interactItr : *oneInteractSetItr) {//for each interaction
                    z3::expr interactExpr = c.bool_val(true);
                    Interaction* oneInteractPointer = this->interactVec[interactItr];
                    auto oneAssignVec = oneInteractPointer->getAssignments();
                    for(auto oneAssignment = oneAssignVec.begin(); oneAssignment != oneAssignVec.end(); oneAssignment++) {
                        int thisParamId = (*oneAssignment)->getParamID();
                        int thisValueId = (*oneAssignment)->getValueID();
                        z3::expr thisAssign = vVec[thisParamId] == c.int_val(thisValueId);
                        interactExpr = interactExpr && thisAssign;
                    }
                    interactSetExprOne = interactSetExprOne || interactExpr;
                }

                z3::expr interactSetExprAnother = c.bool_val(false);
                for(auto anothInteractItr : *anotherInteractSetItr) {
                    z3::expr interactExprAnother = c.bool_val(true);
                    Interaction* anotherInteractPointer = this->interactVec[anothInteractItr];
                    auto anothAssignVec = anotherInteractPointer->getAssignments();
                    for(auto oneAssignment = anothAssignVec.begin(); oneAssignment != anothAssignVec.end(); oneAssignment++) {
                        int thisParamId = (*oneAssignment)->getParamID();
                        int thisValueId = (*oneAssignment)->getValueID();
                        z3::expr thisAssign = vVec[thisParamId] == c.int_val(thisValueId);
                        interactExprAnother = interactExprAnother && thisAssign;
                    }
                    interactSetExprAnother = interactSetExprAnother || interactExprAnother;
                }

                s.add((interactSetExprOne && !interactSetExprAnother) || (interactSetExprAnother && !interactSetExprOne));

                if(s.check() == z3::sat) {
                    numOfDistPair++;
                    std::pair<int, int> distSetPair;
                    distSetPair.first = (oneSetLocate);
                    distSetPair.second = (anotherSetLocate);
//                    std::sort(distSetPair[0], distSetPair[1]);
                    if(distSetPair.first > distSetPair.second) {
                        int temp = distSetPair.second;
                        distSetPair.second = distSetPair.first;
                        distSetPair.first = temp;
                    }
                    this->distLogger.push_back(distSetPair);
                } else {
                    numOfIndisPair++;
                    std::pair<int, int> indisSetPair;
                    indisSetPair.first = (oneSetLocate);
                    indisSetPair.second = (anotherSetLocate);
//                    std::sort(indisSetPair[0], indisSetPair[1]);
                    if(indisSetPair.first > indisSetPair.second) {
                        int temp = indisSetPair.second;
                        indisSetPair.second = indisSetPair.first;
                        indisSetPair.first = temp;
                    }
                    this->indisLogger.push_back(indisSetPair);
                }

                s.pop();
                anotherSetLocate++;
            }
            oneSetLocate++;
        }
    }

    Printer::printTln("The number of valid interaction sets: " + std::to_string(this->interactSetVec.size()), true);
    Printer::printTln("The number of distinguishable pairs: " + std::to_string(numOfDistPair), true);
    Printer::printTln("The number of indistinguishable pairs: " + std::to_string(numOfIndisPair), true);
}
