//
// Created by haojin on 19/01/10.
//

#ifndef COCOLOT_CONSTRAINT_H
#define COCOLOT_CONSTRAINT_H

#include <vector>

typedef enum PropOperator {
    And = 0,
    Or,
    Not,
    If,
    Equal,
    Unequal
} PropOperator;

class Constraint {
private:
    PropOperator thisOperator;
    Constraint* leftOperant = nullptr;
    Constraint* rightOperant = nullptr;
    std::vector<Constraint*> operantsVec;

    bool isParam = false;
    bool isValue = false;
    int paramID = 0;
    int valueID = 0;

public:
    Constraint() {};
    void setOperator(PropOperator thisOpe);
    void setLeftOperant(Constraint* leftConst);
    void setRightOperant(Constraint* rightConst);
    void setOperant(Constraint* newOperant);
    void setIsParam();
    void setIsValue();
    void setParam(int paramId);
    void setValue(int valueId);

    Constraint* getLeftOperant();
    Constraint* getRightOperant();
    PropOperator getThisOperator();
    std::vector<Constraint*> getOperants();
    bool getIsParam();
    bool getIsValue();
    int getParam();
    int getValue();

    ~Constraint();
};


#endif //COCOLOT_CONSTRAINT_H
