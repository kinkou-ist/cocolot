//
// Created by hao jin on 2018-12-27.
//

#ifndef COCOLOT_ASSIGNMENT_H
#define COCOLOT_ASSIGNMENT_H



class Assignment {
private:
    int paramIDIncluded;
    int valueIDIncluded;

public:
    Assignment(const int &paramID, const int &valueID) : paramIDIncluded(paramID), valueIDIncluded(valueID) {};
    int getParamID() { return this->paramIDIncluded; }
    int getValueID() { return this->valueIDIncluded; }
};


#endif //COCOLOT_ASSIGNMENT_H
