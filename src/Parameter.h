//
// Created by k-kou on 18/12/08.
//

#ifndef COCOLOT_PARAMETER_H
#define COCOLOT_PARAMETER_H

#include <iostream>
#include <vector>
#include <unordered_map>
#include "Value.h"


class Parameter {
private:
    int paramID = 0;
    std::string paramName = "";
    int valueNum = 0;
    std::vector<Value> valueVec;
    std::unordered_map<std::string, int> valueNameToIDMap;
public:
    Parameter(const int &paramNum, const std::string &paramName) : paramID(paramNum), paramName(paramName) {};
    void setAllValues(std::vector<std::string> valueNameVec);
    int setAValue(std::string valueName);
    std::string getParamName() const { return this->paramName; }
    int getParamID() const { return this->paramID; }
    int getValueNum() const { return this->valueNum; }
    Value getValueInstance(const int valueID) { return this->valueVec[valueID]; }
};


#endif //COCOLOT_PARAMETER_H
