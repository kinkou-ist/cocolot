//
// Created by hao jin on 2018-11-28.
//

/**
 * Exit Code:
 * 0: Normal Exit
 * 1: Wrong Argument
 * 2: Wrong Execution in Option Handling
 * 3: Fail to Open the Input File Returns 3
 * 4: Wrong Constraint Format
 * 5: Interaction Construction Failed
 * 6: SMT Construction Failed
 */
#include "Printer.h"
#include "OptionHandler.h"
#include "FileParser.h"
#include "Sut.h"
#include "Util.h"

int main(int argc, char *argv[]) {
    /**
     * Main method for the whole program
     */
    bool trueVerbosity = true;
    Printer::printEqualLine(trueVerbosity);
    Printer::printTln("This is COCOLOT", trueVerbosity);
    Printer::printCurrentDateAndTime(trueVerbosity);
    Printer::printEqualLine(trueVerbosity);

    OptionHandler oh(argc, argv);
    oh.printOptions();
    int repeatTimes = oh.getRepeatTimes();

    Sut* sutHead;
    std::vector<std::tuple<int, int, int, long long>> itrRecorder;//< iteration ID, test suite size, original suite size, elapsed time >

    /**
     * Main Process
     */
    FileParser* parser = nullptr;

    //===============================================================================================
    //===============================================================================================
    //Util Parts
    //===============================================================================================
    //===============================================================================================
    if(oh.ifValidCheck()) {
        /**
         * Check whether the input test suite is a valid (d', t')-CLA
         */

        /**
         * Use SmtlaParser to construct an Sut instance to get the number of total valid interactions
         */
        auto modelParser = new SmtlaParser;
        modelParser->parse(oh.getInputFile());
        modelParser->printSystem(trueVerbosity);
        modelParser->printOthers(trueVerbosity);

        sutHead = new Sut;
        sutHead->setParameters(modelParser->getParamVec());
        sutHead->setParamNameVec(modelParser->getParamNameVec());
        sutHead->setParamToValVec(modelParser->getParamToValNumVec());
        sutHead->setConstraints((std::vector<Constraint*>*)modelParser->getOtherPointer());
        sutHead->setCLAParameters(oh.getTValue(), oh.getDValue(), oh.getTJudge(), oh.getDJudge());

        sutHead->ckInteractionSets(false);
        int numOfTotalValidInteract = (int)sutHead->getInteractions().size();
        /**
         * Use CatlaParser to construct Test Case
         */
        //from here
        Printer::printAsterriskLine(true);
        parser = new CatlaParser;
        parser->parse(oh.getTestSuiteFile());
        parser->printOthers(true);

        auto sutHeadNew = new Sut;
        sutHeadNew->setParameters(parser->getParamVec());
        sutHeadNew->setParamNameVec(parser->getParamNameVec());
        sutHeadNew->setParamToValVec(parser->getParamToValNumVec());
        sutHeadNew->setTestSuite((std::vector<TestCase>*)parser->getOtherPointer());
        sutHeadNew->setCLAParameters(oh);
        sutHeadNew->setPrintVerbosity(true);

        int testSuiteSize = (int)sutHeadNew->getTestSuite()->size();
        sutHeadNew->mkInteractionSets();

        auto distSizeOne = sutHead->getDistLogger()->size();
        auto distSizeTwo = sutHeadNew->getDistLogger()->size();
        auto indisSizeOne = sutHead->getIndisLoger()->size();
        auto indisSizeTwo = sutHeadNew->getIndisLoger()->size();

        if((distSizeOne == distSizeTwo) && (indisSizeOne == indisSizeTwo)) {
            bool equalJudgeOne = judgeEquality(sutHead, sutHeadNew, true);
            bool equalJudgeTwo = judgeEquality(sutHeadNew, sutHead, false);
            bool equalJudge = equalJudgeOne && equalJudgeTwo;
            if(equalJudge) {
                std::string printStr;
                printStr += "The test suite is a valid (";
                printStr += std::to_string(oh.getDValue());
                if(oh.getDJudge())  printStr += "'";
                printStr += ",";
                printStr += std::to_string(oh.getTValue());
                if(oh.getTJudge())  printStr += "'";
                printStr += ")-CLA";
                Printer::printTln(printStr, true);
            } else {
                Printer::printTln("Not Valid!", true);
            }
        } else {
            Printer::printTln("Not Valid!", true);
        }

        Printer::printAsterriskLine(true);

        delete parser;
        delete sutHead;
        delete sutHeadNew;
        delete modelParser;
    } else if(oh.ifConversion()) {
        /**
         * Conversion Method
         */
        Printer::printTln("Start Conversion", trueVerbosity);
        if(oh.getOutputFormat() == "bool") {
            if(oh.getInputFormat() == "cnf" || oh.getInputFormat() == "dimacs") {
                parser = new CNFParser;
                parser->parse(oh.getInputFile());
                parser->setCurrentDir(argv[0]);
                parser->printOthers(false);//true -> print in terminal, false -> only print in file
            }
        }
        delete parser;
    } else if(oh.ifMeasureCov()) {
        /**
         * Measure Coverage Method
         */
        /**
         * Use SmtlaParser to construct an Sut instance to get the number of total valid interactions
         */
        auto modelParser = new SmtlaParser;
        modelParser->parse(oh.getInputFile());
        modelParser->printSystem(trueVerbosity);
        modelParser->printOthers(trueVerbosity);

        sutHead = new Sut;
        sutHead->setParameters(modelParser->getParamVec());
        sutHead->setParamNameVec(modelParser->getParamNameVec());
        sutHead->setParamToValVec(modelParser->getParamToValNumVec());
        sutHead->setConstraints((std::vector<Constraint*>*)modelParser->getOtherPointer());
        sutHead->setCLAParameters(oh.getStrength(), 1, false, true);

        sutHead->ckInteractionSets(true);
        int numOfTotalValidInteract = (int)sutHead->getInteractions().size();
        delete sutHead;
        delete modelParser;
        /**
         * Use CatlaParser to construct Test Case
         */
        Printer::printAsterriskLine(true);
        parser = new TestSuiteParser;
        parser->parse(oh.getTestSuiteFile());
        parser->printOthers(true);

        sutHead = new Sut;
        sutHead->setParameters(parser->getParamVec());
        sutHead->setParamNameVec(parser->getParamNameVec());
        sutHead->setParamToValVec(parser->getParamToValNumVec());
        sutHead->setTestSuite((std::vector<TestCase>*)parser->getOtherPointer());
        sutHead->setCLAParameters(oh.getStrength(), 1, false, true);
        sutHead->setPrintVerbosity(false);

        int testSuiteSize = (int)sutHead->getTestSuite()->size();
        sutHead->mkInteractionSets();
        Printer::printAsterriskLine(true);

        auto testCase2InteractMap = sutHead->getCase2InteractsVec();

        std::unordered_set<int> interactCoverLogger;
        int testCaseId = 0;

        for(auto testCase : testCase2InteractMap) {
            int formerSetSize = 0;
            for(auto interactId : testCase) {
                interactCoverLogger.insert(interactId);
            }
            int nowSetSize = (int)interactCoverLogger.size();
            double rate = ((double)nowSetSize / numOfTotalValidInteract) * 100;
            Printer::printTln("Test case " + std::to_string(++testCaseId) + " newly covers " + std::to_string(nowSetSize - formerSetSize) + ": " + std::to_string(rate) + "%", true);
        }

        Printer::printAsterriskLine(true);

        delete parser;
        delete sutHead;

    } else if(oh.ifGeneration()) {
        //===============================================================================================
        //===============================================================================================
        //Generation Parts
        //===============================================================================================
        //===============================================================================================
        if(oh.getExecutionMethod() == "derive") {
            /**
             * Deriving Method
             */
            bool repeatVerbosity = true;
            if(repeatTimes > 10)
                repeatVerbosity = false;
            bool printVerbose = true;

            for(int repeatItr = 0; repeatItr < repeatTimes; repeatItr++) {

                auto itrStart = std::chrono::system_clock::now();

                auto sysFilePath = oh.getInputFile();
                sysFilePath = sysFilePath.substr(sysFilePath.find_last_of("\\/") + 1);
                sysFilePath = sysFilePath.substr(0, sysFilePath.find_first_of("\\."));
                auto sysName = "../inputs/" + sysFilePath + ".cca";
                std::string commandString = "java -jar ../lib/*.jar -i " + oh.getInputFile() + " -c " + std::to_string(oh.getStrength()) + " -o " + sysName;
                std::system(commandString.c_str());

                parser = new CatlaParser;
                parser->parse(sysName);
                parser->printSystem(printVerbose);
                parser->printOthers(repeatVerbosity);//CatlaParser: print test suite; SmtlaParser: print Constraint;
                Printer::printAsterriskLine(printVerbose);
                printVerbose = false;

                sutHead = new Sut;

                sutHead->setParameters(parser->getParamVec());
                sutHead->setParamNameVec(parser->getParamNameVec());
                sutHead->setParamToValVec(parser->getParamToValNumVec());
                sutHead->setTestSuite((std::vector<TestCase>*)parser->getOtherPointer());
                sutHead->setCLAParameters(oh);
                sutHead->setPrintVerbosity(repeatVerbosity);

//                delete parser;
                int originalSuiteSize = (int)sutHead->getTestSuite()->size();

                sutHead->mkInteractionSets();

                std::vector<int> testSuiteRes;
                testSuiteRes = minimizeTestSuite(sutHead->getInteractions(), sutHead->getCase2InteractsVec(), sutHead->getInteract2CasesVec(), sutHead->getOverlineD(), repeatVerbosity, repeatItr);

                int reducedSuiteSize = sutHead->updateTestSuite(testSuiteRes);
                auto itrStop = std::chrono::system_clock::now();
                auto elapsedTimeMainAlgorithm = std::chrono::duration_cast<std::chrono::milliseconds>(itrStop - itrStart).count();
                itrRecorder.push_back(std::make_tuple(repeatItr, reducedSuiteSize, originalSuiteSize, elapsedTimeMainAlgorithm));

                delete sutHead;//not done
                delete parser;//not done
                Printer::printTln("Iteration\t" + std::to_string(repeatItr + 1) + "\tdone", trueVerbosity);

            }

        } else if(oh.getExecutionMethod() == "smt") {
            /**
             * SMT-Based Method
             */
            bool repeatVerbosity = true;
            if(repeatTimes > 10)
                repeatVerbosity = false;
            bool printVerbose = true;

            for(int repeatItr = 0; repeatItr < repeatTimes; repeatItr++) {

                auto itrStart = std::chrono::system_clock::now();

                auto sysFilePath = oh.getInputFile();

                parser = new SmtlaParser;
                parser->parse(sysFilePath);
                parser->printSystem(printVerbose);
                parser->printOthers(printVerbose);//CatlaParser: print test suite; SmtlaParser: print Constraint;
                printVerbose = false;

                sutHead = new Sut;

                sutHead->setParameters(parser->getParamVec());
                sutHead->setParamNameVec(parser->getParamNameVec());
                sutHead->setParamToValVec(parser->getParamToValNumVec());
                sutHead->setConstraints((std::vector<Constraint*>*)parser->getOtherPointer());
                sutHead->setCLAParameters(oh);
                sutHead->setPrintVerbosity(repeatVerbosity);

                sutHead->ckInteractionSets(false);

                std::vector<TestCase> testSuite;
                sysFilePath = sysFilePath.substr(sysFilePath.find_last_of("\\/") + 1);
                sysFilePath = sysFilePath.substr(0, sysFilePath.find_first_of("\\."));
                auto CCAPath = "../inputs/" + sysFilePath + ".cca";
                std::string commandString = "java -jar ../lib/*.jar -i " + oh.getInputFile() + " -c " + std::to_string(oh.getTValue() + oh.getDValue()) + " -o " + CCAPath;
                std::system(commandString.c_str());

                int sizeForCCA = getSizeOfCCA(CCAPath);
                std::vector<std::vector<int>> claGenerated = smtBasedGeneration(sutHead, sizeForCCA, oh.getSymmetryBreakingMethod(), repeatVerbosity);
                auto itrStop = std::chrono::system_clock::now();
                auto elapsedTimeMainAlgorithm = std::chrono::duration_cast<std::chrono::milliseconds>(itrStop - itrStart).count();
                itrRecorder.push_back(std::make_tuple(repeatItr, claGenerated.size(), sizeForCCA, elapsedTimeMainAlgorithm));

                /**
                 * Print CLA generated, be careful about print verbose
                 */
                Printer::printTln("Test Suite[ CLA ] is : ", repeatVerbosity);
                int caseToPrintCounter = 0;

                std::sort(begin(claGenerated), end(claGenerated));

                for(auto tc : claGenerated) {
                    Printer::printT("[\t" + std::to_string(caseToPrintCounter) + "\t]:\t\t", repeatVerbosity);
                    for(int paramItr = 0; paramItr < sutHead->getParamNum(); paramItr++) {
                        if(paramItr != sutHead->getParamNum() - 1) {
                            Printer::print(std::to_string(tc[paramItr]) + "\t\t", repeatVerbosity);
                        } else {
                            Printer::println(std::to_string(tc[paramItr]), repeatVerbosity);
                        }
                    }
                    caseToPrintCounter++;
                }

                Printer::printAsterriskLine(repeatVerbosity);
                delete sutHead;//not done
                delete parser;//not done
                Printer::printTln("Iteration\t" + std::to_string(repeatItr + 1) + "\tdone", trueVerbosity);
            }

        }

        Printer::printDollarLine(trueVerbosity);
        Printer::printTln("Execution Details:", trueVerbosity);

        int reducedSizeTotal = 0;
        long long timeTotal = 0;
        int maxSize = std::get<1>(itrRecorder[0]), minSize = std::get<1>(itrRecorder[0]);
        long long maxTime = std::get<3>(itrRecorder[0]), minTime = std::get<3>(itrRecorder[0]);
        int originalSuiteSizeAve = 0;
        for(auto tupleItr : itrRecorder) {
            int thisSize = std::get<1>(tupleItr);
            long long thisTime = std::get<3>(tupleItr);
            originalSuiteSizeAve += std::get<2>(tupleItr);
            Printer::println("\t\t\t" + std::to_string(thisSize) + " , " + std::to_string(thisTime), trueVerbosity);
            reducedSizeTotal += thisSize;
            timeTotal += thisTime;
            if(thisSize > maxSize)
                maxSize = thisSize;
            if(thisSize < minSize)
                minSize = thisSize;
            if(thisTime > maxTime)
                maxTime = thisTime;
            if(thisTime < minTime)
                minTime = thisTime;
        }

        Printer::printDollarLine(trueVerbosity);
        Printer::printTln("Average(RepeatTime:" + std::to_string(repeatTimes) + "):", trueVerbosity);
        auto averageSize = (int)(reducedSizeTotal / repeatTimes);
        originalSuiteSizeAve = originalSuiteSizeAve / repeatTimes;
        double lessThanOne = (double)reducedSizeTotal / repeatTimes - averageSize;
        if(lessThanOne > 0.5)
            averageSize++;
        auto averageTime = (timeTotal / repeatTimes);
        double lessThanOneTime = (double)timeTotal / repeatTimes - averageTime;
        if(lessThanOneTime > 0.5)
            averageTime++;
        Printer::printTln("\tSize:\t" + std::to_string(averageSize) + "(original suite size\t" + std::to_string(originalSuiteSizeAve) + ")\t, Time:\t" + std::to_string(averageTime), trueVerbosity);
        Printer::printTln("MaxSize:\t" + std::to_string(maxSize) + "\t||\t" + "MinSize:\t" + std::to_string(minSize), trueVerbosity);
        Printer::printTln("MaxTime:\t" + std::to_string(maxTime) + "\t||\t" + "MinTime:\t" + std::to_string(minTime), trueVerbosity);
        Printer::printDollarLine(trueVerbosity);
    }

    Printer::printEqualLine(trueVerbosity);
    Printer::printTln("Terminating...", trueVerbosity);
    Printer::printTln("\tDeleting allocated memory...", trueVerbosity);
    /**
     * Delete memory allocated
     */
    /**
     * TODO: Delete all of the allocated things
     */

    Printer::printTln("Done", trueVerbosity);
    return 0;
}