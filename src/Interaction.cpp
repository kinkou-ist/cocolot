//
// Created by haojin on 19/01/02.
//

#include "Interaction.h"

Interaction::Interaction(std::vector<Assignment *> &tempArray, int interactNum) {
    this->interactID = interactNum;
    this->assignArray.resize(tempArray.size());
    int itr = 0;
    for(auto assignItr : tempArray) {
        this->assignArray[itr] = new Assignment(assignItr->getParamID(), assignItr->getValueID());
        itr++;
    }
}

void Interaction::addCoveringCase(int caseID) {
    this->testCaseSet.insert(caseID);
}

int Interaction::getInteractID() {
    return this->interactID;
}

std::unordered_set<int> Interaction::getCoveringCases() {
    return this->testCaseSet;
}

void Interaction::setDistinguishability(bool dis) {
    this->distinguishability = dis;//true->distinguishable false->indistinguishable
}

bool Interaction::getDistinguishability() {
    return this->distinguishability;//true->distinguishable false->indistinguishable
}

std::vector<Assignment*> Interaction::getAssignments() {
    return this->assignArray;
}

