//
// Created by haojin on 19/01/10.
//

#include "Constraint.h"

void Constraint::setOperator(PropOperator thisOpe) {
    this->thisOperator = thisOpe;
}

void Constraint::setLeftOperant(Constraint *leftConst) {
    this->leftOperant = leftConst;
}

void Constraint::setRightOperant(Constraint *rightConst) {
    this->rightOperant = rightConst;
}

Constraint *Constraint::getLeftOperant() {
    return this->leftOperant;
}

Constraint *Constraint::getRightOperant() {
    return this->rightOperant;
}

PropOperator Constraint::getThisOperator() {
    return this->thisOperator;
}

Constraint::~Constraint() {
    if(this->leftOperant != nullptr) {
        this->leftOperant->~Constraint();
        delete this->leftOperant;
    }
    if(this->rightOperant != nullptr) {
        this->rightOperant->~Constraint();
        delete this->rightOperant;
    }
}

void Constraint::setIsParam() {
    this->isParam = true;
}

void Constraint::setIsValue() {
    this->isValue = true;
}

void Constraint::setParam(int paramId) {
    this->paramID = paramId;
}

void Constraint::setValue(int valueId) {
    this->valueID = valueId;
}

bool Constraint::getIsParam() {
    return this->isParam;
}

bool Constraint::getIsValue() {
    return this->isValue;
}

int Constraint::getParam() {
    return this->paramID;
}

int Constraint::getValue() {
    return this->valueID;
}

void Constraint::setOperant(Constraint *newOperant) {
    this->operantsVec.push_back(newOperant);
}

std::vector<Constraint *> Constraint::getOperants() {
    return this->operantsVec;
}
