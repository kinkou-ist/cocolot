//
// Created by k-kou on 18/12/08.
//
/**
 * Inheritance
 * public: All of the PUBLIC methods and domains in parent class are
 * set to be PUBLIC in child class
 *
 * private: All of the PUBLIC methods and domains in parent class are
 * set to be PRIVATE in child class
 *
 * protected: All of the PUBLIC and PROTECTED methods and domains in parent class are
 * set to be PROTECTED in child class
 */

#ifndef COCOLOT_FILEPARSER_H
#define COCOLOT_FILEPARSER_H

#include <iostream>
#include <vector>
#include "Parameter.h"
#include "TestCase.h"
#include "Constraint.h"

class FileParser {
protected:
    /**
     * Data
     */
    //become protected in child class, cannot be accessed from outside of two classes
    int paramNum = 0;
    std::vector<int> paramToValNumVec;
    std::vector<Parameter> paramVec;
    std::vector<std::string> paramNameVec;
    std::string currentPath;

public:
    /**
     * Method
     */
    virtual void parse(std::string filePath) = 0;//pure virtual function do not necessarily need to be implemented in child class in C++.
    virtual void printSystem(bool verbose);
    virtual void printOthers(bool verbose) = 0;
    virtual void* getOtherPointer() = 0;
    int getParamNum() { return this->paramNum; };
    std::vector<int>* getParamToValNumVec() { return &this->paramToValNumVec; };
    std::vector<Parameter>* getParamVec() { return &this->paramVec; };
    std::vector<std::string>* getParamNameVec() { return &this->paramNameVec; };
    void setCurrentDir(std::string dir) { this->currentPath = dir.substr(0, dir.substr(0, dir.find_last_of('/')).find_last_of('/')); };
    std::string getCurrentDir() { return this->currentPath; };
    virtual ~FileParser() {};//default?
};

class CatlaParser : public FileParser {
private:
    int caseNum = 0;
    std::vector<TestCase> testSuite;//the same as test suite of CCA

public:
    void parse(std::string filePath) override;
    void printOthers(bool verbose) override;//print test suite
    void* getOtherPointer() override;
    ~CatlaParser() {};
};

class SmtlaParser : public FileParser {
private:
    /**
     * Data
     */
    int constNum = 0;
    std::vector<Constraint*> constVec;

public:
    void parse(std::string filePath) override;
    void printOthers(bool verbose) override;//print test suite
    void* getOtherPointer() override;
    ~SmtlaParser() {};
};

class CNFParser : public FileParser {
private:
    std::string filePath;
    int constNum = 0;
    int paramNum = 0;
    std::vector<std::string> constStrVec;
public:
    void parse(std::string filePath) override;
    void printOthers(bool verbose) override;
    void* getOtherPointer() override;
};

class TestSuiteParser : public FileParser {
private:
    int caseNum = 0;
    std::vector<TestCase> testSuite;//the same as test suite of CCA

public:
    void parse(std::string filePath) override;
    void printOthers(bool verbose) override;//print test suite
    void* getOtherPointer() override;
    ~TestSuiteParser() {};
};

#endif //COCOLOT_FILEPARSER_H
