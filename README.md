# COCOLOT

COCOLOT is an executable C++ program for CIT(**C**ombinatorial **I**nteraction **T**esting).
COCOLOT is an abbreviation for **CO**nstrained **CO**vering and **LO**cating Array **T**ool.

Basically, all of the algorithms in the published papers would be added into this tool.

---

## Functions

1. Generating (d',t')-CLAs with SMT-solver;
2. Generating (d',t')-CLAs with deriving method;
3. Check validity for (d',t')-CLAs;

---

## Libraries

As the COCOLOT tool would use several libraries for calculation and checking, be sure all of the libraries are installed correctly in the OS.

1. [Z3](https://github.com/Z3Prover/z3), Leonardo de Moura and Nikolaj Bjørner, Microsoft Research, an SMT solver;
2. [Boost](https://www.boost.org/), Boost Library, using <boost/program_options>;
3. [CIT-BACH](http://www-ise4.ist.osaka-u.ac.jp/~t-tutiya/CIT/), CIT-BACH, the Constrained Covering Array generator;

---

## Options

### General

Main functions:

- `--help (-h)` 

Print help message;

- `--version (-v)` 

Print version;

- `--model (-m) [arg]` 

Path `[arg]` to model file;

- `--dimension-d (-d) [arg]` 

Dimension `[arg]` for interaction sets Constrained Locating Arrays;

- `--overline-D (-D)`

If set, all interaction sets up to sizes `--dimension-d` would be taken into account;

- `--strength-t (-t) [arg]`

Strength `[arg]` for interactions in Constrained Locating Arrays;

- `--overline-T (-T)`

If set, all interactions up tp strength `--strength-t` would be taken into account;

- `--method-keyword (-k) [arg]`

Keywords for generation methods, SMT-based method: smt, Test suite based method: derive;

- `--repeat (-r) [arg]`

If set, the generation would be repeated for `[arg]` times;

### SMT-based Method Options

- `--symmetry-break (-b) [arg]`

If set, corresponding symmetry breaking method would be adapted, None: 0, Lexicographic Order: 1, Pre-assignments: 2;

### Utility Function Options

- `--convert-format (-C) [arg]`

Convert the `model` file into `[arg]` file format, Bool format: bool;

- `--measure (-M)`

Measure the coverage of the input test suites based on `model` file, `input` and `strength` is depended;

- `--input (-i) [arg]`

Path `[arg]` to the input test suite;


---

## Reference Papers

1. Constrained locating arrays for combinatorial interaction testing, arXiv:1801.06041 [cs.SE], [link](https://arxiv.org/abs/1801.06041).
2. A Satisfiability-Based Approach to Generation of Constrained Locating Arrays, ICSTW2018, [link](https://ieeexplore.ieee.org/document/8411765).
3. Deriving Fault Locating Test Cases from Constrained Covering Arrays, PRDC2018, link.

---

Edited: 19, Feb., 2019. Hao Jin.