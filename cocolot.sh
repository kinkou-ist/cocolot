#!/usr/bin/env bash
TIME=/usr/bin/time
which /usr/bin/time || TIME=gtime

TIMEOUT=timeout
which timeout || TIMEOUT=gtimeout

#get project root directory
currentDir=$PWD
inputDir=${currentDir}/models
outputDir=${currentDir}/outputs
binDir=${currentDir}/cmake_build

if [ ! -d ${binDir} ]; then
    cd ${currentDir}
    ./build.sh
elif [ -d ${binDir} ]; then
    echo "bin directory found"
    ./build.sh clean
    cd ${currentDir}
    ./build.sh
fi

#if [ -z "$1" ]; then
#    cd ${inputDir}
#    for file_name in *.txt; do
#        echo ${file_name}
#        ${TIME} -v ${TIMEOUT} 3600 ${binDir}/COCOLOT -m ${file_name} -s 3 -d 1 -t 2 -D -k derive -r 10 1> ${outputDir}/${file_name%%.*}.cla 2> ${outputDir}/${file_name%%.*}.time
##        ${binDir}/COCOLOT -m ${inputDir}/${file_name} -c bool
#    done
#fi
#
#if [ -z "$1" ]; then
#    cd ${inputDir}
#    for file_name in *.txt; do
#        echo ${file_name}
#        ${TIME} -v ${TIMEOUT} 3600 ${binDir}/COCOLOT -m ${file_name} -s 3 -d 1 -t 2 -D -k derive -r 10 1> ${outputDir}/2${file_name%%.*}.cla 2> ${outputDir}/2${file_name%%.*}.time
##        ${binDir}/COCOLOT -m ${inputDir}/${file_name} -c bool
#    done
#fi

cd ${inputDir}
${TIME} -v ${binDir}/COCOLOT -m ${inputDir}/zzzapache.txt -s 3 -d 1 -t 2 -D -k derive -r 1 1> ${outputDir}/3zzzapache.cla 2> ${outputDir}/3zzzapache.time
${TIME} -v ${binDir}/COCOLOT -m ${inputDir}/zzzgcc.txt -s 3 -d 1 -t 2 -D -k derive -r 1 1> ${outputDir}/3zzzgcc.cla 2> ${outputDir}/3zzzgcc.time