#!/usr/bin/env bash
PROJECT="COCOLOT"
CMAKEDIR=./cmake_build

if [ -x "$(command -v cmake3)" ]; then
    CMAKE=cmake3
elif [ -x "$(command -v cmake)" ]; then
    CMAKE=cmake
else
    echo "cmake command not found!"
    exit 1
fi

if [ "$1" = "clean" ]; then
    rm -rf cmake_build
    rm -f $PROJECT
    rm -rf cmake*
    rm -rf ./src/*.gch
    rm -rf .idea
elif [ -z "$1" ]; then
    #statements
    rm -rf cmake_build
    rm -f $PROJECT
    rm -rf cmake*
    rm -rf ./src/*.gch
    rm -rf .idea

    mkdir $CMAKEDIR
    cd $CMAKEDIR
    ${CMAKE} ..
    make
fi
